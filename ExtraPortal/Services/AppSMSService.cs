﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ExtraPortal.Models;
using Dapper;

namespace ExtraPortal.Services
{
    public class AppSMSService
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();

        public IEnumerable<IndividualSMSModel> GetIndividualContactOption()
        {
            var result = _db.Query<IndividualSMSModel>(";Exec GetIndividualContactOption");
            return result;
        }

        public IEnumerable<IndividualSMSModel> GetCorporateContactOption()
        {
            var result = _db.Query<IndividualSMSModel>(";Exec GetCorporateContactOption");
            return result;
        }

        public IEnumerable<IndividualSMSModel> GetGroupContactOption()
        {
            var result = _db.Query<IndividualSMSModel>(";Exec GetGroupContactOption");
            return result;
        }

        public IEnumerable<BantuContactSMSModel> GetBantuContactOption()
        {
            var result = _bdb.Query<BantuContactSMSModel>(";Exec GetBantuContactOption");
            return result;
        }

        public IEnumerable<ProductModel> GetSBProducts()
        {
            var result = _bdb.Query<ProductModel>(";Exec GetSBProduct");
            return result;
        }

        public void CreateCustomSMS(CustomSmsModel model)
        {
            foreach (var ContactID in model.IndividualContactID)
            {
                string strClientId = _db.Query<string>("SaveIndividualSMS", new
                {
                    ContactID = ContactID,
                    Message = model.Message,
                    Author = HttpContext.Current.Session["KE_UserName"],
                    Terminus = Environment.MachineName,
                    IpAddress = HttpContext.Current.Request.UserHostAddress
                }, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }
        }

    }
}