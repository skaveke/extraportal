﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using System.Data;
using Dapper;

namespace ExtraPortal.Services
{
    public class SupremeMobileService
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();

        public List<SupremeUsersListModel> GetSupremeUsers()
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetSupremeUsers");
            return result.ToList();
        }
        public List<SupremeUsersListModel> GetPendingSupremeUsers()
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetPendingSupremeUsers");
            return result.ToList();
        }

        public List<SupremeUsersListModel> GetBantuUsersOption(string Prefix)
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetBantuUsersOption @Prefix",
                    new
                    {
                        Prefix = Prefix
                    });
            return result.ToList();
        }

        public SupremeUsersListModel GetBantuUserDetails(string Operator)
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetBantuUserDetails @OperatorID",
                    new
                    {
                        OperatorID = Operator
                    }).SingleOrDefault();
            return result;
        }

        public List<SupremeUsersListModel> SupremeUsersList()
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetSupremeUsersList");
            return result.ToList();
        }

        public void CreateSupremeUser(SupremeUsersModel model)
        {
            string EPassword = null;
            EPassword = Library.EncodePassword(model.UserName + model.Password).ToString();//Encrypt Password
            try
            {
                _bdb.Query<int>(";Exec CreateSupremeUser @UserName,@ClientID,@EmployeeID,@Password,@OurBranchID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        UserName = model.UserName,
                        ClientID = model.ClientID,
                        EmployeeID = model.EmployeeID,
                        Password = EPassword,
                        OurBranchID = "00",
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ApproveSupremeUser(SupremeUsersListModel model)
        {           
            try
            {
                var message = _bdb.Query<int>(";Exec ApproveSupremeUser @UserName,@Author,@Terminus,@IpAddress",
                    new
                    {
                        UserName = model.UserName,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public string BlockSupremeUser(SupremeUsersListModel model)
        {
            try
            {
                var message = _bdb.Query<int>(";Exec BlockSupremeUser @UserName,@Author,@Terminus,@IpAddress",
                    new
                    {
                        UserName = model.UserName,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public string UnblockSupremeUser(SupremeUsersListModel model)
        {
            try
            {
                var message = _bdb.Query<int>(";Exec UnblockSupremeUser @UserName,@Author,@Terminus,@IpAddress",
                    new
                    {
                        UserName = model.UserName,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public List<DeviceAssignmentListModel> GetDevicesAssignedList()
        {
            var result = _bdb.Query<DeviceAssignmentListModel>(";Exec GetDevicesAssignedList");
            return result.ToList();
        }

        public List<SupremeUsersListModel> GetSupremeUserOption()
        {
            var result = _bdb.Query<SupremeUsersListModel>(";Exec GetSupremeUserOption");
            return result.ToList();
        }
        
        public List<SupremeDevicesListModel> GetSupremeDevicesOption()
        {
            var result = _bdb.Query<SupremeDevicesListModel>(";Exec GetSupremeDevicesOption");
            return result.ToList();
        }

        public string AssignUserDevice(DeviceAssignmentModel model)
        {
            try
            {
                var message = _bdb.Query<int>(";Exec AssignUserDevice @UserName,@DeviceID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        UserName = model.UserName,
                        DeviceID = model.DeviceID,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

    }
}