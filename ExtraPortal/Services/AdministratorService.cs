﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using System.Data;
using Dapper;

namespace ExtraPortal.Services
{
    public class AdministratorService
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();
        CRMService _CRMService = new CRMService();

        public List<ActivityLogListModel> GetActivityLogList(PagingModel model)
        {
            var result = _db.Query<ActivityLogListModel>(";Exec GetActivityLogList @pageIndex,@pageSize",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize
                    });
            return result.ToList();
        }
        public string GetActivityLogCount()
        {
            var result = _db.Query<CountModel>(";Exec GetActivityLogCount").SingleOrDefault();
            return result.totalCount.ToString();
        }

    }
}