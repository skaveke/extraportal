﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using Dapper;
using System.Data;


namespace ExtraPortal.Services
{
    public class USSDService
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();
        CRMService _CRMService = new CRMService();

        public List<IndividualContactsListModel> GetUSSDSubscribers()
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetUSSDSubscribers");
            return result.ToList();
        }

        public List<BantuContactsListModel> GetUSSDSubscriberOption(string Prefix)
        {
            var result = _db.Query<BantuContactsListModel>(";Exec GetUSSDSubscriberOption @Prefix",
                    new
                    {
                        Prefix = Prefix
                    });
            return result.ToList();
        }

        public List<BantuContactsListModel> GetDisabledUSSDSubscriberOption(string Prefix)
        {
            var result = _db.Query<BantuContactsListModel>(";Exec GetDiabledUSSDSubscriberOption @Prefix",
                    new
                    {
                        Prefix = Prefix
                    });
            return result.ToList();
        }

        public IndividualContactsListModel GetUSSDSubscriberDetails(string ClientID)
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetUSSDSubscriberDetails @ClientID",
                    new
                    {
                        ClientID = ClientID
                    }).SingleOrDefault();
            return result;
        }

        public void DisableUSSDSubscriber(DisableUSSDSubscribersModel model)
        {
            try
            {
                _db.Query<int>(";Exec DisableUSSDSubscriber @ContactID,@BlockedReason,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        BlockedReason = model.BlockedReason,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EnableUSSDSubscriber(EnableUSSDSubscribersModel model)
        {
            string Pin = null;
            string EPin = null;

            Pin = _CRMService.GenerateRandomNo().ToString();//Generate random 4 digit number
            EPin = Library.EncodePassword(DataTypeExtensions.Right(model.Mobile, 9) + Pin).ToString();//Encrypt PIN

            try
            {
                _db.Query<int>(";Exec EnableUSSDSubscriber @ContactID,@Mobile,@Pin,@EPin,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        Mobile = model.Mobile,
                        Pin = Pin,
                        EPin = EPin,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public void ChangePIN(PINManagementModel model)
        {
            string EPin = null;
            EPin = Library.EncodePassword(DataTypeExtensions.Right(model.Mobile, 9) + model.Pin).ToString();//Encrypt PIN

            try
            {
                _db.Query<int>(";Exec ChangePIN @ContactID,@Mobile,@Pin,@EPin,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        Mobile = model.Mobile,
                        Pin = model.Pin,
                        EPin = EPin,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UserChangePIN(UserPINManagementModel model)
        {
            string EPin = null;
            EPin = Library.EncodePassword(DataTypeExtensions.Right(model.Mobile, 9) + model.Pin).ToString();//Encrypt PIN

            try
            {
                var message =  _db.Query<int>(";Exec UserUSSDChangePIN @Mobile,@Pin,@EPin",
                    new
                    {
                        Mobile = model.Mobile,
                        Pin = model.Pin,
                        EPin = EPin

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        
        public IndividualContactsListModel GetDisabledUSSDSubscriberDetails(string ClientID)
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetDisabledUSSDSubscriberDetails @ClientID",
                    new
                    {
                        ClientID = ClientID
                    }).SingleOrDefault();
            return result;
        }

        public List<IndividualContactsListModel> GetDisabledUSSDSubscribersList()
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetDisabledUSSDSubscribersList");
            return result.ToList();
        }

        public string GetBlockedSubscribersCount()
        {
            var result = _db.Query<DisableEnableUSSDSubscribersModel>(";Exec GetBlockedSubscribersCount").SingleOrDefault();
            return result.BlockedCount.ToString();
        }

        public List<IndividualContactsListModel> GetPendingApproveEnabledUSSDSubscribersList()
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetPendingApproveEnabledUSSDSubscribersList");
            return result.ToList();
        }

        public string GetPendingApproveEnabledSubscribersCount()
        {
            var result = _db.Query<DisableEnableUSSDSubscribersModel>(";Exec GetPendingApproveEnabledSubscribersCount").SingleOrDefault();
            return result.BlockedCount.ToString();
        }
                
        public string ApproveUnblocking(CustomerApprovalModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec ApproveUnblocking @ContactID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }

    }
}