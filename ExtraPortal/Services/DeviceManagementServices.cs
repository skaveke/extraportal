﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using System.Data;
using Dapper;

namespace ExtraPortal.Services
{
    public class DeviceManagementServices
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();

        public List<SupremeDevicesListModel> GetSupremeDevices()
        {
            var result = _bdb.Query<SupremeDevicesListModel>(";Exec GetSupremeDevices");
            return result.ToList();
        }

        public string SaveEditSupremeDevice(SupremeDevicesModel model)
        {
            try
            {
                var message = _bdb.Query<int>(";Exec SaveEditSupremeDevice @DeviceNumber,@DeviceID,@DeviceModel,@ProductName,@AndroidVersion,@ProductType,@Memory,@Storage,@Speed,@Size,@Weight,@Color,@Author,@Terminus,@IpAddress,@IsAdd",
                    new
                    {
                        DeviceNumber = model.DeviceNumber,
                        DeviceID = model.DeviceID,
                        DeviceModel = model.DeviceModel,
                        ProductName = model.ProductName,
                        AndroidVersion = model.AndroidVersion,
                        ProductType = model.ProductType,
                        Memory = model.Memory,
                        Storage = model.Storage,
                        Speed = model.Speed,                         
                        Size = model.Size,
                        Weight = model.Weight,
                        Color = model.Color,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress,
                        IsAdd = model.IsAdd
                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        
    }
}