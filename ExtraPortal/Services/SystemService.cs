﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using Dapper;
using System.Data;
namespace ExtraPortal.Services
{
    public class SystemService
    {
        static IDbConnection _db = SystemTools.DbCon();
        static IDbConnection _bdb = SystemTools.B_DbCon();
        static public IEnumerable<BranchModel> SystemBranch(string BankID)
        {
            var Branches = _bdb.Query<BranchModel>("pc_SearchSystemBranches",
                new
                {
                    BankID = BankID
                },
                null, true, 0, commandType: CommandType.StoredProcedure);

            return Branches;

        }

        public IEnumerable<MainModuleModel> MainModules()
        {
            var result = _db.Query<MainModuleModel>(";Exec GetMainModules");
            return result;
        }

        public IEnumerable<ModuleModel> Modules()
        {
            var result = _db.Query<ModuleModel>(";Exec GetModules");
            return result;
        }

        static public void UpDateLogin(LoginUpdateModel model)
        {
            _bdb.Execute("p_UpdateLoginStatus", new
            {
                OurBranchID = model.OurBranchID,
                OperatorID = model.OperatorID,
                Success = model.Success,
                IPAddress = model.IPAddress,
                MessageID = "1017"
            }, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<smsTypeModel> GetSMSType()
        {
            List<smsTypeModel> report = new List<smsTypeModel> {
                new smsTypeModel{ TypeID=1,Description = "Free SMS(No Charges)"},
                new smsTypeModel{ TypeID=2,Description = "Charge Client Saving Account"}
                };
            return report;
        }
    }
}