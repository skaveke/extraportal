﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtraPortal.Models;
using Dapper;
using System.Data;

namespace ExtraPortal.Services
{
    public class CRMService
    {
        IDbConnection _db = SystemTools.DbCon();

        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();

        public List<BantuContactsListModel> GetBantuContactsList(PagingModel model)
        {
            var result = _bdb.Query<BantuContactsListModel>(";Exec GetBantuContactsList @pageIndex,@pageSize",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize
                    });
            return result.ToList();
        }
        public List<BantuContactsListModel> GetBantuContactsOption(string Prefix)
        {
            var result = _bdb.Query<BantuContactsListModel>(";Exec GetBantuContactsOption @Prefix",
                    new
                    {
                        Prefix = Prefix
                    });
            return result.ToList();
        }
        public string GetBantuContactsCount()
        {
            var result = _bdb.Query<CountModel>(";Exec GetBantuContactsCount").SingleOrDefault();
            return result.totalCount.ToString();
        }
        public string SaveEditIndividualContact(IndividualContactsModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec SaveEditIndividualContact @ContactID,@ClientID,@ServiceType,@ClientType,@Name,@Gender,@Occupation,@NationalID,@Mobile,@Email,@Address,@Author,@Terminus,@IpAddress,@IsAdd,@Entry",
                    new
                    {                        
                        ContactID = model.ContactID,
                        ClientID = model.ClientID,
                        ServiceType = model.ServiceType,
                        ClientType = model.ClientType,
                        Name = model.Name,
                        Gender = model.Gender,
                        Occupation = model.Occupation,
                        NationalID = model.NationalID, 
                        Mobile = model.Mobile.Replace(" ", ""), //Remove white space
                        Email = model.Email,
                        Address = model.Address,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress,
                        IsAdd = model.IsAdd,
                        Entry = model.Entry
                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public List<IndividualContactsListModel> GetIndividualContactsList(PagingModel model)
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetIndividualContactsList @pageIndex,@pageSize",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize
                    });
            return result.ToList();
        }
        public string GetIndividualContactsCount()
        {
            var result = _db.Query<CountModel>(";Exec GetIndividualContactsCount").SingleOrDefault();
            return result.totalCount.ToString();
        }
        public List<IndividualContactsListModel> IndividualContactsList()
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetIndividualContacts");
            return result.ToList();
        }
        public string RemoveIndividualContactDetails(IndividualContactsListModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec RemoveIndividualContact @ContactID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public string ApproveCustomer(IndividualContactsListModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec ApproveCustomer @ContactID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        ContactID = model.ContactID,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public List<IndividualContactsListModel> GetCorporateContactsList(PagingModel model)
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetCorporateContactsList @pageIndex,@pageSize",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize
                    });
            return result.ToList();
        }
        public string GetCorporateContactsCount()
        {
            var result = _db.Query<CountModel>(";Exec GetCorporateContactsCount").SingleOrDefault();
            return result.totalCount.ToString();
        }
        public List<GroupsListModel> GetGroupsList(PagingModel model)
        {
            var result = _db.Query<GroupsListModel>(";Exec GetGroupsList @pageIndex,@pageSize",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize
                    });
            return result.ToList();
        }
        public string GetGroupsCount()
        {
            var result = _db.Query<CountModel>(";Exec GetGroupsCount").SingleOrDefault();
            return result.totalCount.ToString();
        }
        public string SaveEditGroup(GroupsListModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec SaveEditGroup @GroupID,@GroupName,@Description,@Author,@Terminus,@IpAddress,@IsAdd",
                    new
                    {
                        GroupID = model.GroupID,
                        GroupName = model.GroupName,
                        Description = model.Description,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress,
                        IsAdd = model.IsAdd
                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public List<GroupsListModel> GetGroupDetails()
        {
            var result = _db.Query<GroupsListModel>(";Exec GetGroupDetails");
            return result.ToList();
        }
        public string RemoveGroup(GroupsListModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec RemoveGroup @GroupID,@Author,@Terminus,@IpAddress",
                    new
                    {
                        GroupID = model.GroupID,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public List<IndividualContactsListModel> GetGroupContactsList(PagingModel model)
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetGroupContactsList @pageIndex,@pageSize,@id",
                    new
                    {
                        pageIndex = model.pageIndex,
                        pageSize = model.pageSize,
                        id = model.id
                    });
            return result.ToList();
        }
        public string GetGroupContactsCount(string id)
        {
            var result = _db.Query<CountModel>(";Exec GetGroupContactsCount @id",
                    new
                    {
                        id = id
                    }).SingleOrDefault();
            return result.totalCount.ToString();
        }
        public string SaveEditGroupContact(IndividualContactsListModel model)
        {
            try
            {
                var message = _db.Query<int>(";Exec SaveEditGroupContact @ContactID,@GroupID,@Name,@Mobile,@Author,@Terminus,@IpAddress,@IsAdd",
                    new
                    {
                        ContactID = model.ContactID,
                        GroupID = model.GroupID,
                        Name = model.Name,
                        Mobile = model.Mobile,
                        Author = HttpContext.Current.Session["KE_UserName"],
                        Terminus = Environment.MachineName,
                        IpAddress = HttpContext.Current.Request.UserHostAddress,
                        IsAdd = model.IsAdd
                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public IndividualContactsListModel GetClientDetails(string ClientID)
        {
            var result = _bdb.Query<IndividualContactsListModel>(";Exec GetClientDetails @ClientID",
                    new
                    {
                        ClientID = ClientID
                    }).SingleOrDefault();
            return result;
            
        }
        public List<IndividualContactsListModel> CustomerListPendingApproval()
        {
            var result = _db.Query<IndividualContactsListModel>(";Exec GetCustomerListPendingApproval");
            return result.ToList();
        }

        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
        public string ApproveCustomer(CustomerApprovalModel model)
        {
            string Pin = null;
            string EPin = null;
            //if service type is USSD
            if (model.ServiceType == "USSD")
            {
                Pin = GenerateRandomNo().ToString();//Generate random 4 digit number
                EPin = Library.EncodePassword(DataTypeExtensions.Right(model.Mobile, 9) + Pin).ToString();//Encrypt PIN
                try
                {
                    var message = _db.Query<int>(";Exec ApproveCustomer @ContactID,@Mobile,@Pin,@EPin,@Author,@Terminus,@IpAddress",
                        new
                        {
                            ContactID = model.ContactID,
                            Mobile = model.Mobile,
                            Pin = Pin,
                            EPin = EPin,
                            Author = HttpContext.Current.Session["KE_UserName"],
                            Terminus = Environment.MachineName,
                            IpAddress = HttpContext.Current.Request.UserHostAddress

                        }).SingleOrDefault();
                    return message.ToString();
                }
                catch (Exception ex)
                {
                    return ex.Message.ToString();
                }
            }

            //Other service
            else {

                try
                {
                    var message = _db.Query<int>(";Exec ApproveCustomer @ContactID,@Mobile,@Pin,@EPin,@Author,@Terminus,@IpAddress",
                         new
                         {
                             ContactID = model.ContactID,
                             Mobile = model.Mobile,
                             Pin = Pin,
                             EPin = EPin,
                             Author = HttpContext.Current.Session["KE_UserName"],
                             Terminus = Environment.MachineName,
                             IpAddress = HttpContext.Current.Request.UserHostAddress

                         }).SingleOrDefault();
                    return message.ToString();
                }
                catch (Exception ex)
                {
                    return ex.Message.ToString();
                }

            }         
            
        }

    }
}