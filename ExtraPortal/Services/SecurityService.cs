﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Models;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace ExtraPortal.Services
{
    public class SecurityService
    {
        IDbConnection _db = SystemTools.DbCon();
        static IDbConnection _bdb = SystemTools.B_DbCon();
        SystemTools _SystemTools = new SystemTools();
        static public AuthenticateModel UserAuthenticate(LoginModel Umodel)
        {
            var Authenticate = _bdb.Query<AuthenticateModel>("p_Authenticateuser",
              new
              {
                  OurBranchID = Umodel.OurBranchID,
                  OperatorID = Umodel.UserName.ToUpper(),
                  IPAddress = SystemTools.GetInternalIP()
              },
              null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

            return Authenticate;
        }        
        public void TokenUpdate(LoginModel Tokenmodel, string token)
        {
            _db.Query(";Exec UpdateUserToken @UserID,@TokenID,@SecurityStamp,@Terminus,@IpAddress",
                new
                {
                    UserID = Tokenmodel.UserID,
                    TokenID = token,
                    SecurityStamp = Tokenmodel.SecurityStamp,
                    Terminus = Environment.MachineName,
                    IpAddress = HttpContext.Current.Request.UserHostAddress
                });
        }
        public void DestroyToken(string token)
        {
            _db.Query(";Exec DestroyToken @TokenID",
                new
                {
                    TokenID = token
                });
        }                
        public class SessionTimeoutAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {

                HttpContext ctx = HttpContext.Current;

                if (HttpContext.Current.Session["KE_UserName"] == null && HttpContext.Current.Session["KE_RoleID"] == null)
                {
                    filterContext.Result = new RedirectResult("~/Security/Login");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
        }
        public UserDetailsModel DisplayUserInfo(string token)
        {
            UserDetailsModel deptList = _db.Query<UserDetailsModel>(";Exec UserDisplay @TokenID",
                new
                {
                    TokenID = token,
                }).Single();

            return deptList;
        }
        public void TokenUpdate(UserModel Tokenmodel, string token)
        {
            _db.Query(";Exec UpdateUserUSSDToken @UserName,@TokenID,@SecurityStamp",
                new
                {
                    UserName = Tokenmodel.UserName,
                    TokenID = token,
                    SecurityStamp = Tokenmodel.SecurityStamp
                });
        }                 
        public ValidTokenResultModel TokenAuthenticate(string validToken)
        {
            var loginResult = _db.Query<ValidTokenResultModel>(";Exec AuthenticateTokenUssd @TokenID",
                new
                {
                    TokenID = validToken
                }).SingleOrDefault();

            return loginResult;
        }
        public void AddLoginUser(NewUsersModel nUsermodel)
        {
            string mobile = DataTypeExtensions.Right(nUsermodel.UserName, 9);
            _db.Query<NewUsersModel>(";Exec AddAuthenticateUssdUser @UserName,@Password",
                new
                {
                    UserName = nUsermodel.UserName,
                    Password = Library.EncodePassword(DataTypeExtensions.Right(nUsermodel.UserName, 9) + nUsermodel.Password)
                }).SingleOrDefault();

        }
        public pinModel fetchClientPIN(string _PhoneNumber)
        {
            try
            {
                var myResult = _db.Query<pinModel>(";EXEC p_CustomerUSSDPin @MobileNo",
                    new
                    {
                        MobileNo = _PhoneNumber
                    }).SingleOrDefault();

                return myResult;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
        public accountModel fetchClientDetail(string ClientID)
        {
            try
            {
                var myResult = _bdb.Query<accountModel>(";EXEC p_CustomerUSSDInfo @ClientID",
                    new
                    {
                        ClientID = ClientID
                    }).SingleOrDefault();

                return myResult;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
        public Clientaccounts fetchAccounts(ClientRequest model)
        {
            Clientaccounts myResult = new Clientaccounts();
            try
            {
                if (model.AccountTypeID == "S")
                {

                    myResult = _bdb.Query<Clientaccounts>(";EXEC p_CustomerAccountSaving @OurBranchID,@ClientID",
                       new
                       {
                           OurBranchID = model.BranchID,
                           ClientID = model.ClientID
                       }).SingleOrDefault();
                }
                else
                {

                    myResult = _bdb.Query<Clientaccounts>(";EXEC p_CustomerAccountloan @OurBranchID,@ClientID",
                       new
                       {
                           OurBranchID = model.BranchID,
                           ClientID = model.ClientID
                       }).SingleOrDefault();
                }

                return myResult;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
        public ClientExist ClientCheck(string _PhoneNumber)
        {
            try
            {
                var myResult = _db.Query<ClientExist>(";EXEC p_CustomerUSSDExist @MobileNo",
                    new
                    {
                        MobileNo = _PhoneNumber
                    }).SingleOrDefault();

                return myResult;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
        public List<Statement> fetchAccountsStatement(string _AccountID)
        {

            try
            {

                var myResult = _bdb.Query<Statement>(";EXEC p_GetAccountStatementUssd @AccountID",
                      new
                      {
                          AccountID = _AccountID
                      }).ToList();
                return myResult;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
    }
}