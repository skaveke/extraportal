﻿using System.Web;
using System.Web.Optimization;

namespace ExtraPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Include all minified css styles
            bundles.Add(new StyleBundle("~/assets/extraportal").IncludeDirectory("~/Assets/css", "*.min.css"));

            //include custom css and Js
            bundles.Add(new StyleBundle("~/assets/custom").Include(
                "~/Assets/css/custom.css",
                "~/Assets/plugins/select2/select2.min.css"
                ));

            bundles.Add(new ScriptBundle("~/scripts/mandatory").Include(
                "~/Assets/js/libs/jquery-2.1.1.min.js",
                "~/Assets/js/libs/jquery-ui-1.10.3.min.js"));

            bundles.Add(new ScriptBundle("~/scripts/angular").Include(
                "~/Scripts/angular.min.js",
                "~/Scripts/Resources/ui-bootstrap-tpls-0.13.4.min.js"));

            bundles.Add(new ScriptBundle("~/scripts/extraportal").Include(
               "~/Assets/js/app.config.js",
               "~/Assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js",
               "~/Assets/js/bootstrap/bootstrap.min.js",
               "~/Assets/js/notification/SmartNotification.min.js",
               "~/Assets/js/smartwidgets/jarvis.widget.min.js",
               "~/Assets/js/plugin/jquery-validate/jquery.validate.min.js",
               "~/Assets/js/plugin/masked-input/jquery.maskedinput.min.js",
               "~/Assets/js/plugin/select2/select2.min.js",
               "~/Assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js",
               "~/Assets/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
               "~/Assets/js/plugin/msie-fix/jquery.mb.browser.min.js",
               "~/Assets/js/plugin/fastclick/fastclick.min.js",
               "~/Assets/js/app.min.js"));

            bundles.Add(new ScriptBundle("~/scripts/plugins").Include(
                "~/Assets/plugins/select2/select2.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/full-calendar").Include(
                "~/Assets/js/plugin/moment/moment.min.js",
                "~/Assets/js/plugin/fullcalendar/jquery.fullcalendar.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/charts").Include(
                "~/Assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js",
                "~/Assets/js/plugin/sparkline/jquery.sparkline.min.js",
                "~/Assets/js/plugin/morris/morris.min.js",
                "~/Assets/js/plugin/morris/raphael.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.cust.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.resize.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.time.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.fillbetween.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.orderBar.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.pie.min.js",
                "~/Assets/js/plugin/flot/jquery.flot.tooltip.min.js",
                "~/Assets/js/plugin/dygraphs/dygraph-combined.min.js",
                "~/Assets/js/plugin/chartjs/chart.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/datatables").Include(
                "~/Assets/js/plugin/datatables/jquery.dataTables.min.js",
                "~/Assets/js/plugin/datatables/dataTables.colVis.min.js",
                "~/Assets/js/plugin/datatables/dataTables.tableTools.min.js",
                "~/Assets/js/plugin/datatables/dataTables.bootstrap.min.js",
                "~/Assets/js/plugin/datatable-responsive/datatables.responsive.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/jq-grid").Include(
                "~/Assets/js/plugin/jqgrid/jquery.jqGrid.min.js",
                "~/Assets/js/plugin/jqgrid/grid.locale-en.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/forms").Include(
                "~/Assets/js/plugin/jquery-form/jquery-form.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/smart-chat").Include(
                "~/Assets/js/smart-chat-ui/smart.chat.ui.min.js",
                "~/Assets/js/smart-chat-ui/smart.chat.manager.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/vector-map").Include(
                "~/Assets/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js",
                "~/Assets/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"
                ));

            //bundles.Add(new ScriptBundle("~/scripts/crm").Include(
            //    "~/Angular/CRM/Contacts.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
