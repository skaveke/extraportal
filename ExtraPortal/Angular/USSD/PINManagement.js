﻿var app = angular.module("pinmgtApp", ['ui.bootstrap']);
app.controller("pinmgtCtrl", function ($scope, $http) {
    //Get USSD Subscriber Details
    $scope.GetClientDetails = function () {
        var ClientID = $("#ClientName").val();
        $http.get("/AppUSSD/GetUSSDSubscriberDetails?ClientID=" + ClientID).then(
            function (response) {
                $("#ContactID").val(response.data.ContactID);
                $scope.Name = response.data.Name;
                $scope.Mobile = response.data.Mobile;
            },
            function (err) {
                var error = err;
            });
    }
    
});