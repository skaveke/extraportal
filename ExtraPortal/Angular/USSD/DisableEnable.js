﻿var app = angular.module("disableenableApp", ['ui.bootstrap']);
app.controller("disableenableCtrl", function ($scope, $http) { 
    //Get USSD Subscriber Details
    $scope.GetClientDetails = function () {
        var ClientID = $("#ClientName").val();
        $http.get("/AppUSSD/GetUSSDSubscriberDetails?ClientID=" + ClientID).then(
            function (response) {
                $("#DContactID").val(response.data.ContactID);
                $scope.DName = response.data.Name;
                $scope.DPhone = response.data.Phone1;
                $scope.DMobile = response.data.Mobile;
                $("#DGender").val(response.data.Gender);
            },
            function (err) {
                var error = err;
            });
    }

    //Get USSD Subscriber Details
    $scope.GetEClientDetails = function () {
        var ClientID = $("#EClientName").val();        
        $http.get("/AppUSSD/GetDisabledUSSDSubscriberDetails?ClientID=" + ClientID).then(
            function (response) {
                $("#EContactID").val(response.data.ContactID);
                $scope.EName = response.data.Name;
                $scope.EPhone = response.data.Phone1;
                $scope.EMobile = response.data.Mobile;
                $("#EGender").val(response.data.Gender);
            },
            function (err) {
                var error = err;
            });
    }
});