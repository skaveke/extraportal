﻿var app = angular.module("activitylogApp", ['ui.bootstrap']);
app.controller("activitylogCtrl", function ($scope, $http) {

    //------------------Begin: Individual Contacts---------------------------
    $scope.maxSize = 10;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page. 

    $scope.getActivityLogList = function () {
        $http.get("/Administrator/GetActivityLogList?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected).then(
            function (response) {
                $scope.activityLog = response.data.activityLogList;
                $scope.totalCount = response.data.totalCount;
            },
            function (err) {
                var error = err;
            });
    }

    $scope.getActivityLogList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getActivityLogList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getActivityLogList();
    };

   
});