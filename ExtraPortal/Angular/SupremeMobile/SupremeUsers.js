﻿var app = angular.module("userApp", ['ui.bootstrap']);
app.controller("userCtrl", function ($scope, $http) {
    //Get Bantu User Details
    $scope.GetUserDetails = function () {
        var Operator = $("#Operator").val();
        $http.get("/AppSupremeMobile/GetBantuUserDetails?Operator=" + Operator).then(
            function (response) {
                $("#UserName").val(response.data.OperatorID);
                $("#ClientID").val(response.data.ClientID);
                $("#EmployeeID").val(response.data.EmployeeID);
            },
            function (err) {
                var error = err;
            });
    }       
});