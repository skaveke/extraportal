﻿var app = angular.module("devicesApp", ['ui.bootstrap']);
app.controller("devicesCtrl", function ($scope, $http) {
    
    //Get Device Details
    $scope.getDeviceDetails = function (DeviceNumber) {
        $http.get("/DeviceManagement/GetSupremeDeviceDetails?DeviceNumber=" + DeviceNumber).then(
            function (response) {
                $scope.Edit1 = false;
                $scope.Edit2 = true;
                $scope.Edit3 = false;
                $scope.IsAdd = "0";
                $scope.DeviceNumber = response.data.DeviceNumber;
                $scope.DeviceModel = response.data.DeviceModel;
                $scope.DeviceID = response.data.DeviceID;
                $scope.ProductName = response.data.ProductName;
                $scope.AndroidVersion = response.data.AndroidVersion;
                $("#ProductType").val(response.data.ProductType);
                $scope.Memory = response.data.Memory;
                $scope.Storage = response.data.Storage;
                $scope.Speed = response.data.Speed;
                $scope.Size = response.data.Size;
                $scope.Weight = response.data.Weight;
                $scope.Color = response.data.Color;
                $scope.CreatedBy = response.data.CreatedBy;
                $scope.CreatedDate = response.data.CreatedDate;
                $scope.EditedBy = response.data.EditedBy;
                $scope.EditedDate = response.data.EditedDate;
                //$scope.ApprovedBy = response.data.ApprovedBy;
                //$scope.DateApproved = response.data.DateApproved;
                $scope.Edit = false;
                $('#response').html("");
            },
            function (err) {
                var error = err;
            });
    }

    $scope.ClearAll = function () {
        $scope.Reset();
        $('#response').html("");
    }

    $scope.Reset = function () {
        $scope.Edit = false;
        $scope.ClearFields();
        $scope.Edit1 = false;
        $scope.Edit2 = false;
        $scope.Edit3 = false;
    }

    $scope.EditDetails = function () {
        $scope.Edit = true;
        $scope.Edit2 = false;
        $scope.Edit3 = true;
    }

    $scope.ClearFields = function () {
        $scope.IsAdd = "1";
        $scope.DeviceNumber = "";
        $scope.DeviceModel = "";
        $scope.DeviceID = "";
        $scope.ProductName = "";
        $scope.AndroidVersion = "";
        $scope.Memory = "";
        $scope.Storage = "";
        $scope.Speed = "";
        $scope.Size = "";
        $scope.Weight = "";
        $scope.Color = "";
    }

    //Save/Edit Device Details
    $scope.SaveEditSupremeDevice = function () {
        $('#response').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");
        $scope.Dev = {};
        $scope.Dev.IsAdd = $scope.IsAdd;
        $scope.Dev.DeviceNumber = $scope.DeviceNumber;
        $scope.Dev.DeviceModel = $scope.DeviceModel;
        $scope.Dev.DeviceID = $scope.DeviceID;
        $scope.Dev.ProductName = $scope.ProductName;
        $scope.Dev.AndroidVersion = $scope.AndroidVersion;
        $scope.Dev.ProductType = $("#ProductType").val();
        $scope.Dev.Memory = $scope.Memory;
        $scope.Dev.Storage = $scope.Storage;
        $scope.Dev.Speed = $scope.Speed;
        $scope.Dev.Size = $scope.Size;
        $scope.Dev.Weight = $scope.Weight;
        $scope.Dev.Color = $scope.Color;

        $http({
            method: "post",
            url: "/DeviceManagement/SaveEditSupremeDevice",
            datatype: "json",
            data: JSON.stringify($scope.Dev)
        }).then(function (response) {
            if (response.data == '1') {
                $('#response').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.Reset();
            } else {
                $('#response').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

});