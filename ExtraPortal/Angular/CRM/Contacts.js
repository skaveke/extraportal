﻿var app = angular.module("contactsApp", ['ui.bootstrap']);
app.controller("contactsCtrl", function ($scope, $http) {    
    //------------------Begin: Individual Contacts---------------------------
    $scope.Ind_maxSize = 10;     // Limit number for pagination display number.
    $scope.Ind_totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.Ind_pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.Ind_pageSizeSelected = 10; // Maximum number of items per page. 

    $scope.getIndividualContactsList = function () {
        $http.get("/CRM/GetIndividualContactsList?pageIndex=" + $scope.Ind_pageIndex + "&pageSize=" + $scope.Ind_pageSizeSelected).then(
            function (response) {
                $scope.individualContacts = response.data.individualContactsList;
                $scope.Ind_totalCount = response.data.totalCount;
            },
            function (err) {
                var error = err;
            });
    }

    $scope.getIndividualContactsList();

    //This method is calling from pagination number
    $scope.Ind_pageChanged = function () {
        $scope.getIndividualContactsList();
    };

    //This method is calling from dropDown
    $scope.Ind_changePageSize = function () {
        $scope.Ind_pageIndex = 1;
        $scope.getIndividualContactsList();
    };
    
    $scope.ClearAllInd = function () {
        $scope.ResetInd();
        $('#Indresponse').html("");
    }

    $scope.ResetInd = function () {
        $scope.IndEdit = false;
        $scope.ClearIndFields();
        $scope.IndEdit1 = true;
        $scope.IndEdit2 = false;
        $scope.IndEdit3 = false;
        //$scope.IndEdit4 = false;
    }

    $scope.AddInd = function () {
        $scope.IndEdit = true;
        $scope.ClearIndFields();
        $scope.IndEdit1 = false;
        $scope.IndEdit2 = false;
        $scope.IndEdit3 = false;
    }

    $scope.EditInd = function () {
        $scope.IndEdit = true;
        $scope.IndEdit2 = false;
        $scope.IndEdit3 = true;

        //$scope.IndEdit4 = true;
    }

    $scope.ClearIndFields = function () {
        $scope.ContactID = "0";
        $scope.IndIsAdd = "1";
        $scope.Name = "";
        $scope.NationalID = "";
        $scope.Mobile = "";
        $scope.Email = "";
        $scope.Address = "";
    }

    //Save/Edit IndividualContact Details
    $scope.SaveEditIndividualContact = function () {
        $('#Indresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");
        $scope.IndL = {};
        $scope.IndL.IsAdd = $scope.IndIsAdd;
        $scope.IndL.Entry = $("#Entry").val();
        $scope.IndL.ContactID = $scope.ContactID; 
        $scope.IndL.ClientID = $("#ClientName").val();
        $scope.IndL.ServiceType = $("#ServiceType").val();
        $scope.IndL.ClientType = $scope.ClientType;
        $scope.IndL.Name = $scope.Name;
        $scope.IndL.NationalID = $scope.NationalID;
        $scope.IndL.Mobile = $scope.Mobile;
        $scope.IndL.Gender = $("#Gender").val();
        $scope.IndL.Occupation = $("#Occupation").val();
        $scope.IndL.Email = $scope.Email;
        $scope.IndL.Address = $scope.Address;
        $http({
            method: "post",
            url: "/CRM/SaveEditIndividualContact ",
            datatype: "json",
            data: JSON.stringify($scope.IndL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#Indresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.ResetInd();
                $scope.getIndividualContactsList();
            } else {
                $('#Indresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //Get Individual Contact Details
    $scope.getIndividualContactDetails = function (Ind) {
        var ContactID = Ind.ContactID;
        $http.get("/CRM/GetIndividualContactDetails?ContactID=" + ContactID).then(
            function (response) {
                $scope.IndEdit1 = false;
                $scope.IndEdit2 = true;
                $scope.IndEdit3 = false;
                //$scope.IndEdit4 = false;
                $scope.IndIsAdd = "0";
                $scope.ContactID = response.data.ContactID;
                $scope.Name = response.data.Name;
                $scope.NationalID = response.data.NationalID;
                $scope.Mobile = response.data.Mobile;
                $("#ServiceType").val(response.data.ServiceType);
                $("#Gender").val(response.data.Gender);
                $("#Occupation").val(response.data.Occupation);
                $scope.Email = response.data.Email;
                $scope.Address = response.data.Address;
                $scope.CreatedBy = response.data.CreatedBy;
                $scope.ApprovedBy = response.data.ApprovedBy;
                $scope.EditedBy = response.data.EditedBy;
                $scope.DateCreated = response.data.DateCreated;
                $scope.DateApproved = response.data.DateApproved;
                $scope.DateEdited = response.data.DateEdited;
                $scope.IndEdit = false;
                $('#Indresponse').html("");
            },
            function (err) {
                var error = err;
            });
    }

    //Delete Individual Contact
    $scope.RemoveIndContact = function () {
        $('#Indresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");

        $scope.IndL = {};
        $scope.IndL.ContactID = $scope.ContactID;
        $http({
            method: "post",
            url: "/CRM/RemoveIndividualContactDetails",
            datatype: "json",
            data: JSON.stringify($scope.IndL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#Indresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.ResetInd();
                $scope.getIndividualContactsList();
            } else {
                $('#Indresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //Approve Customer
    //$scope.ApproveCustomer = function () {

    //    $('#Indresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");

    //    $scope.IndL = {};
    //    $scope.IndL.ContactID = $scope.ContactID;
    //    $http({
    //        method: "post",
    //        url: "/CRM/ApproveCustomer",
    //        datatype: "json",
    //        data: JSON.stringify($scope.IndL)
    //    }).then(function (response) {
    //        if (response.data == '1') {
    //            $('#Indresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
    //            $scope.ResetInd();
    //            $scope.getIndividualContactsList();
    //        } else {
    //            $('#Indresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
    //        }

    //    })
    //}

    //------------------END: Individual Contacts---------------------------
  

    //------------------Begin: Corporate Contacts---------------------------
    $scope.Cor_maxSize = 10;     // Limit number for pagination display number.
    $scope.Cor_totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.Cor_pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.Cor_pageSizeSelected = 10; // Maximum number of items per page. 

    $scope.getCorporateContactsList = function () {
        $http.get("/CRM/GetCorporateContactsList?pageIndex=" + $scope.Cor_pageIndex + "&pageSize=" + $scope.Cor_pageSizeSelected).then(
            function (response) {
                $scope.corporateContacts = response.data.individualContactsList;
                $scope.Cor_totalCount = response.data.totalCount;
            },
            function (err) {
                var error = err;
            });
    }

    $scope.getCorporateContactsList();

    //This method is calling from pagination number
    $scope.Cor_pageChanged = function () {
        $scope.getCorporateContactsList();
    };

    //This method is calling from dropDown
    $scope.Cor_changePageSize = function () {
        $scope.Cor_pageIndex = 1;
        $scope.getCorporateContactsList();
    };

    $scope.ClearAllCor = function () {
        $scope.ResetCor();
        $('#Corresponse').html("");
    }

    $scope.ResetCor = function () {
        $scope.CorEdit = false;
        $scope.ClearCorFields();
        $scope.CorEdit1 = true;
        $scope.CorEdit2 = false;
        $scope.CorEdit3 = false;
    }

    $scope.AddCor = function () {
        $scope.CorEdit = true;
        $scope.ClearCorFields();
        $scope.CorEdit1 = false;
        $scope.CorEdit2 = false;
        $scope.CorEdit3 = false;
    }

    $scope.EditCor = function () {
        $scope.CorEdit = true;
        $scope.CorEdit2 = false;
        $scope.CorEdit3 = true;
    }

    $scope.ClearCorFields = function () {
        $scope.CorContactID = "0";
        $scope.CorIsAdd = "1";
        $scope.CorName = "";
        $scope.CorPhone = "";
        $scope.CorMobile = "";
        $scope.CorEmail = "";
        $scope.CorAddress = "";
    }

    //Save/Edit IndividualContact Details
    $scope.SaveEditCorporateContact = function () {
        $('#Corresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");
        $scope.CorL = {};
        $scope.CorL.IsAdd = $scope.CorIsAdd;
        $scope.CorL.ContactID = $scope.CorContactID;
        $scope.CorL.ClientType = $('#CorClientType').val();
        $scope.CorL.Name = $scope.CorName;
        $scope.CorL.NationalID = $scope.CorPhone;
        $scope.CorL.Mobile = $scope.CorMobile;
        $scope.CorL.Email = $scope.CorEmail;
        $scope.CorL.Address = $scope.CorAddress;
        $http({
            method: "post",
            url: "/CRM/SaveEditIndividualContact ",
            datatype: "json",
            data: JSON.stringify($scope.CorL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#Corresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.ResetCor();
                $scope.getCorporateContactsList();
            } else {
                $('#Corresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //Get Corporate Contact Details
    $scope.getCorporateContactDetails = function (Cor) {
        var ContactID = Cor.ContactID;
        $http.get("/CRM/GetIndividualContactDetails?ContactID=" + ContactID).then(
            function (response) {
                $scope.CorEdit1 = false;
                $scope.CorEdit2 = true;
                $scope.CorEdit3 = false;
                $scope.CorIsAdd = "0";
                $scope.CorContactID = response.data.ContactID;
                $scope.CorName = response.data.Name;
                $scope.CorPhone = response.data.NationalID;
                $scope.CorMobile = response.data.Mobile;
                $scope.CorEmail = response.data.Email;
                $scope.CorAddress = response.data.Address;
                $scope.CorEdit = false;
                $('#Corresponse').html("");
            },
            function (err) {
                var error = err;
            });
    }

    //Delete Corporate Contact
    $scope.RemoveCorContact = function () {
        $('#Corresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");

        $scope.CorL = {};
        $scope.CorL.ContactID = $scope.CorContactID;
        $http({
            method: "post",
            url: "/CRM/RemoveIndividualContactDetails",
            datatype: "json",
            data: JSON.stringify($scope.CorL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#Corresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.ResetCor();
                $scope.getCorporateContactsList();
            } else {
                $('#Corresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }
    //------------------END: Corporate Contacts---------------------------

    //------------------Begin: Groups ---------------------------
    $scope.g_maxSize = 5;     // Limit number for pagination display number.
    $scope.g_totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.g_pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.g_pageSizeSelected = 5; // Maximum number of items per page. 

    $scope.getGroupsList = function () {
        $http.get("/CRM/GetGroupsList?pageIndex=" + $scope.g_pageIndex + "&pageSize=" + $scope.g_pageSizeSelected).then(
            function (response) {
                $scope.groups = response.data.groupsList;
                $scope.g_totalCount = response.data.totalCount;
            },
            function (err) {
                var error = err;
            });
    }

    $scope.getGroupsList();

    //This method is calling from pagination number
    $scope.g_pageChanged = function () {
        $scope.getGroupsList();
    };

    //This method is calling from dropDown
    $scope.g_changePageSize = function () {
        $scope.g_pageIndex = 1;
        $scope.getGroupsList();
    };

    $scope.ClearAllg = function () {
        $scope.Resetg();
        $('#gresponse').html("");
    }

    $scope.Resetg = function () {
        $scope.gEdit = false;
        $scope.CleargFields();
        $scope.gEdit1 = false;
        $scope.gEdit2 = false;
        $scope.gEdit3 = false;
    }

    $scope.Addg = function () {
        $scope.gEdit = true;
        $scope.CleargFields();
        $scope.gEdit1 = true;
        $scope.gEdit2 = false;
        $scope.gEdit3 = false;
    }

    $scope.Editg = function () {
        $scope.gEdit = true;
        $scope.gEdit2 = false;
        $scope.gEdit3 = true;
    }

    $scope.CleargFields = function () {
        $scope.GroupID = "0";
        $scope.gIsAdd = "1";
        $scope.GroupName = "";
        $scope.Description = "";
    }

    //Save/Edit Group Details
    $scope.SaveEditGroup = function () {
        $('#gresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");
        $scope.gL = {};
        $scope.gL.IsAdd = $scope.gIsAdd;
        $scope.gL.GroupID = $scope.GroupID;
        $scope.gL.GroupName = $scope.GroupName;
        $scope.gL.Description = $scope.Description;
        $http({
            method: "post",
            url: "/CRM/SaveEditGroup",
            datatype: "json",
            data: JSON.stringify($scope.gL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#gresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.Resetg();
                $scope.getGroupsList();
            } else {
                $('#gresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //Get Group Details
    $scope.getGroupDetails = function (grp) {
        var GroupID = grp.GroupID;
        $http.get("/CRM/GetGroupDetails?GroupID=" + GroupID).then(
            function (response) {
                $scope.gEdit1 = true;
                $scope.gEdit2 = true;
                $scope.gEdit3 = false;
                $scope.gIsAdd = "0";
                $scope.GroupID = response.data.GroupID;
                $scope.GroupName = response.data.GroupName;
                $scope.Description = response.data.Description;
                $scope.gEdit = false;
                $scope.Editg();
                $('#gresponse').html("");
            },
            function (err) {
                var error = err;
            });
    }

    //Delete Group
    $scope.RemoveGroup = function () {
        $('#gresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");

        $scope.gL = {};
        $scope.gL.GroupID = $scope.GroupID;
        $http({
            method: "post",
            url: "/CRM/RemoveGroup",
            datatype: "json",
            data: JSON.stringify($scope.gL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#gresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.Resetg();
                $scope.getGroupsList();
            } else {
                $('#gresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //------------------END: Groups---------------------------

    //------------------Begin: Group Contacts ---------------------------
    $scope.gc_maxSize = 10;     // Limit number for pagination display number.
    $scope.gc_totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.gc_pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.gc_pageSizeSelected = 10; // Maximum number of items per page.  

    $scope.getGroupContacts = function (grpc) {
        var id = grpc.GroupID;
        $('#grpName').html("<label><strong>" + grpc.GroupName + "</strong></label>");
        $scope.GrpID = id;
        $scope.grpName = grpc.GroupName;
        $http.get("/CRM/GetGroupContactsList?id=" + id + "&pageIndex=" + $scope.gc_pageIndex + "&pageSize=" + $scope.gc_pageSizeSelected).then(
            function (response) {
                $scope.grpcontacts = response.data.individualContactsList;
                $scope.gc_totalCount = response.data.totalCount;
            },
            function (err) {
                var error = err;
            });
    }

    $scope.Addgc = function () {
        $scope.gcEdit = true;
        $scope.CleargcFields();
        $scope.gcEdit1 = true;
        $scope.gcEdit2 = false;
        $scope.gcEdit3 = false;
    }

    $scope.CleargcFields = function () {
        $scope.GContactID = "0";
        $scope.gcIsAdd = "1";
        $scope.gcName = "";
        $scope.gcContact = "";
    }

    //Save/Edit Group Contact Details
    $scope.SaveEditGroupContact = function () {
        $('#grcresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");
        $scope.gcL = {};
        $scope.gcL.IsAdd = $scope.gcIsAdd;
        $scope.gcL.ContactID = $scope.GContactID;
        $scope.gcL.GroupID = $scope.GrpID;
        $scope.gcL.Name = $scope.gcName;
        $scope.gcL.NationalID = $scope.gcContact;
        $scope.gcL.GroupName = $scope.grpName;
        $http({
            method: "post",
            url: "/CRM/SaveEditGroupContact",
            datatype: "json",
            data: JSON.stringify($scope.gcL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#grcresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.Resetgc();
                $scope.getGroupContacts($scope.gcL);
            } else {
                $('#grcresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    $scope.Resetgc = function () {
        $scope.gcEdit = false;
        $scope.CleargcFields();
        $scope.gcEdit1 = false;
        $scope.gcEdit2 = false;
        $scope.gcEdit3 = false;
    }

    //Get Group Contact Details
    $scope.getGroupContactDetails = function (grpc) {
        var ContactID = grpc.ContactID;
        $http.get("/CRM/GetGroupContactDetails?ContactID=" + ContactID).then(
            function (response) {
                $scope.gcEdit1 = true;
                $scope.gcEdit2 = true;
                $scope.gcEdit3 = false;
                $scope.gcIsAdd = "0";
                $scope.GContactID = response.data.ContactID;
                $scope.gcName = response.data.Name;
                $scope.gcContact = response.data.NationalID;
                $scope.gcEdit = false;
                $scope.Editgc();
                $('#gcresponse').html("");
            },
            function (err) {
                var error = err;
            });
    }

    $scope.Editgc = function () {
        $scope.gcEdit = true;
        $scope.gcEdit2 = false;
        $scope.gcEdit3 = true;
    }
    
    //Delete Group Contact
    $scope.RemoveGroupContact = function () {
        $('#gcresponse').html("<span class='label label-default'><i class='fa fa-cog fa-spin'></i> Processing...</span>");

        $scope.gcL = {};
        $scope.gcL.ContactID = $scope.GContactID;
        $scope.gcL.GroupID = $scope.GrpID;
        $scope.gcL.GroupName = $scope.grpName;
        $http({
            method: "post",
            url: "/CRM/RemoveGroupContact",
            datatype: "json",
            data: JSON.stringify($scope.gcL)
        }).then(function (response) {
            if (response.data == '1') {
                $('#gcresponse').html("<span class='label label-success'><i class='fa fa-check'></i> Success</span>");
                $scope.Resetgc();
                $scope.getGroupContacts($scope.gcL);
            } else {
                $('#gcresponse').html("<span class='label label-danger'><i class='fa fa-exclamation-triangle'></i>" + response.data + "</span>");
            }

        })
    }

    //Get Individual Contact Details
    $scope.GetClientDetails = function () {
        var ClientID = $("#ClientName").val();
        $http.get("/CRM/GetClientDetails?ClientID=" + ClientID).then(
            function (response) {
                $scope.IndIsAdd = "1";
                //$scope.ClientID = response.data.ClientID;
                $scope.Name = response.data.Name;
                $scope.NationalID = response.data.Phone1;
                $scope.Mobile = response.data.Mobile;
                $("#Gender").val(response.data.Gender);
                $scope.Email = response.data.Email;
                $scope.Address = response.data.Address1;
                $scope.IndEdit = true;
                $scope.IndEdit1 = false;
            },
            function (err) {
                var error = err;
            });
    }
});