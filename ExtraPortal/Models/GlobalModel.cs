﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class GlobalModel
    {
    }
    public class CountModel
    {
        public string totalCount { get; set; }
        public string id { get; set; }

    }
    public class PagingModel
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string id { get; set; }

    }
}