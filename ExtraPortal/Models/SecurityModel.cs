﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class GenericResultModel
    {
        public string Status { get; set; }
        public string Remarks { get; set; }
    }
    public class MyUserResultModel
    {
        public string Status { get; set; }
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
    }
    public class ValidTokenResultModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string TokenDate { get; set; }
        public string SecurityStamp { get; set; }
    }
    public class MyTokenResultModel
    {
        public string tokenAuth { get; set; }
    }
    public class LoginModel
    {
        public Int32 UserID { get; set; }
        public string Email { get; set; }
        public string BankID { get; set; }
        [Required(ErrorMessage = "OurBranchID is required")]
        public string OurBranchID { get; set; }
        [Required(ErrorMessage = "UserName is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        public string MachineIP { get; set; }
        public string SecurityStamp { get; set; }
        public string IPAddress { get; set; }
        public IEnumerable<BranchModel> BranchList { get; set; }
    }
    public class LoginResultModel
    {
        public string Status { get; set; }
        public Int32 UserID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string tokenAuth { get; set; }
    }
    public class TokenModel
    {
        public string TokenID { get; set; }
    }
    public class AuthenticateModel
    {
        public string LanguageID { get; set; }
        public string Password { get; set; }
        public int? Accesslevel { get; set; }
        public bool IsLoggedIn { get; set; }
        public DateTime? TimeLoggedIn { get; set; }
        public string IPAddress { get; set; }
        public int? FailedAttempts { get; set; }
        public string LoginFailureIPAddress { get; set; }
        public bool ChangePasswordAtNextLogon { get; set; }
        public bool CanNotChangePassword { get; set; }
        public bool ShowTips { get; set; }
        public bool ShowRecent { get; set; }
        public bool ShowFavorite { get; set; }
        public string RoleID { get; set; }
        public bool Status { get; set; }
    }
    public class LoginUpdateModel
    {
        public string OurBranchID { get; set; }
        public string OperatorID { get; set; }
        public string Success { get; set; }
        public string IPAddress { get; set; }
        public string MessageID { get; set; }

    }

}