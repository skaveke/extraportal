﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExtraPortal.Models
{
    public class SupremeMobileModel
    {
    }
    public class SupremeUsersModel
    {        
        public string OperatorID { get; set; }

        [Required(ErrorMessage = "No User was selected")]
        [Remote("IsUserNameExists", "AppSupremeMobile", HttpMethod = "POST", ErrorMessage = "Username already exists!")]
        public string UserName { get; set; }
        public string EmployeeID { get; set; }
        public string ClientID { get; set; }
        public string Address { get; set; }
        public string DeviceID { get; set; }
        //[Required(ErrorMessage = "OurBranchID is required")]
        public string OurBranchID { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password Confirmation is required")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password Confirmation must match Password.")]
        public string CPassword { get; set; }
        public List<SupremeUsersListModel> supremeUsersList { get; set; }
        public List<SupremeUsersListModel> PendingSupremeUsersList { get; set; }
        public IEnumerable<BranchModel> BranchList { get; set; }


    }
    public class SupremeUsersListModel
    {
        public string OperatorID { get; set; }
        public string UserName { get; set; }
        public string EmployeeID { get; set; }
        public string ClientID { get; set; }
        public string Address { get; set; }
        public string DeviceID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedON { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public int Blocked { get; set; }
    }
    public class SupremeDevicesModel
    {
        [Required(ErrorMessage = "Device Number is Required")]
        public string DeviceNumber { get; set; }
        public string UserName { get; set; }
        public string DeviceModel { get; set; }
        [Required(ErrorMessage = "Device ID is Required")]
        public string DeviceID { get; set; }
        public string AndroidVersion { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string Memory { get; set; }
        public string Storage { get; set; }
        public string Speed { get; set; }
        public string Size { get; set; }
        public string Weight { get; set; }
        public string Color { get; set; }
        public List<SupremeDevicesListModel> supremeDevicesList { get; set; }
        public string IsAdd { get; set; }
    }
    public class SupremeDevicesListModel
    {        
        public string DeviceNumber { get; set; }
        public string DeviceDetails { get; set; }
        public string UserName { get; set; }
        public string DeviceModel { get; set; }        
        public string DeviceID { get; set; }
        public string AndroidVersion { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string Memory { get; set; }
        public string Storage { get; set; }
        public string Speed { get; set; }
        public string Size { get; set; }
        public string Weight { get; set; }
        public string Color { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string EditedBy { get; set; }
        public string EditedDate { get; set; }
    }
    public class DeviceAssignmentModel
    {
        [Required(ErrorMessage = "No User was selected")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "No Device was selected")]
        public string DeviceID { get; set; }
        public List<DeviceAssignmentListModel> deviceAssignmentList { get; set; }
        public List<SupremeUsersListModel> supremeUsers { get; set; }
        public List<SupremeDevicesListModel> supremeDevices { get; set; }

    }
    public class DeviceAssignmentListModel
    {
        public string UserName { get; set; }
        public string DeviceID { get; set; }
        public string DeviceDetails { get; set; }
    }

}