﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class SMSModel
    {
    }
    public class CustomSMSModel
    {
        public string IndividualContactID { get; set; }
        public string CorporateContactID { get; set; }
        public string GroupContactID { get; set; }
        public IEnumerable<IndividualSMSModel> individualContact { get; set; }
        public IEnumerable<IndividualSMSModel> corporateContact { get; set; }
        public IEnumerable<IndividualSMSModel> groupContact { get; set; }
        public IEnumerable<BantuContactSMSModel> Bcsm { get; set; }
    }
    public class IndividualSMSModel
    {
        public int ContactID { get; set; }
        public string Name { get; set; }
    }
    public class BantuContactSMSModel
    {
        public int ClientID { get; set; }
        public string Name { get; set; }
    }

    public class CategorySmsModel
    {
        [Required(ErrorMessage = "ProductID is required")]
        public string ProductID { get; set; }        
        public string Message { get; set; }
        public Int16? TypeID { get; set; }
        public decimal ChargeAmount { get; set; }
        public string OurBranchID { get; set; }
        public IEnumerable<BranchModel> BranchList { get; set; }
        public IEnumerable<smsTypeModel> chargeoption { get; set; }
        //public bool AllBranches { get; set; }
        public IEnumerable<ProductModel> productlist { get; set; }
        //public IEnumerable<BranchModel> BranchList { get; set; }
    }
    public class ProductSmsModel
    {
        [Required(ErrorMessage = "ProductID is required")]
        public string ProductID { get; set; }
        public decimal? Charges { get; set; }
        [Required(ErrorMessage = "Message is required")]
        public string Message { get; set; }
        public Int16? TypeID { get; set; }
        public decimal ChargeAmount { get; set; }
        public IEnumerable<smsTypeModel> chargeoption { get; set; }
        //public bool AllBranches { get; set; }
        public IEnumerable<ProductModel> productlist { get; set; }
        //public IEnumerable<BranchModel> BranchList { get; set; }
    }
    public class smsTypeModel
    {
        public Int16? TypeID { get; set; }
        public string Description { get; set; }
    }
    public class CustomSmsModel
    {
        [Required(ErrorMessage = "Message is required")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        //public string IndividualContactID { get; set; }
        [Required(ErrorMessage = "Message is required")]
        public List<string> IndividualContactID { get; set; }
    }
    public class ProductModel
    {
        public string ProductID { get; set; }
        public string Description { get; set; }
    }
}