﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class _MyUserResultModel
    {
        public string Pin { get; set; }
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
    }

    public class NewUsersModel
    {
        [Required(ErrorMessage = "UserName is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password   is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public Boolean IsApproved { get; set; }
        public string OperatorStatusID { get; set; }
    }
    public class UserModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SecurityStamp { get; set; }
        public string IPAddress { get; set; }
    }

    public class _ValidTokenResultModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string TokenDate { get; set; }
        public string SecurityStamp { get; set; }
    }

    public class _MyTokenResultModel
    {
        public string tokenAuth { get; set; }
    }

    public class _GenericResultModel
    {
        public string Status { get; set; }
        public string Remarks { get; set; }
    }

    public class UserDetailsModel
    {
        public string AppName { get; set; }
        public string SurName { get; set; }
        public string greeting { get; set; }
        public string Year { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string CreatedOn { get; set; }
    }

    public class clientinfo
    {
        public string Pin { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }

    }
    public class accountModel
    {
        public string Pin { get; set; }
        public string OurBranchID { get; set; }
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public string Name { get; set; }
        public decimal? ClearBalance { get; set; }
        
    }

    public class Clientaccounts
    {
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string AccountID { get; set; }
        public decimal? ClearBalance { get; set; }
        public string ProductName { get; set; }
        public string ProductID { get; set; }

    }

    public class ClientRequest
    {
        [Required]
        public string AccountTypeID { get; set; }
        [Required]
        public string BranchID { get; set; }
        [Required]
        public string ClientID { get; set; }

    }
    public class PINParam
    {
        public string PhoneNumber { get; set; }
    }

    public class PINModel
    {
        public Int32 PIN { get; set; }
    }

    public class TokenResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
    }

    public class Statement
    {
        public string TrxDate { get; set; }
        public string Remarks { get; set; }
        public string Amount { get; set; }
    }

    public class StatementRequest
    {
        [Required]
        public string AccountID { get; set; }

    }

}