﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace ExtraPortal.Models
{
    public class CRMModel
    {
    }
    public class BantuContactsModel
    {        
        public List<BantuContactsListModel> bantuContactsList { get; set; }
        public string totalCount { get; set; }
    }
    public class BantuContactsListModel
    {
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Mobile { get; set; }
    }
    public class IndividualContactsModel
    {
        public string IsAdd { get; set; }
        public string Entry { get; set; }
        public int ContactID { get; set; }
        public string ClientID { get; set; }
        public string ServiceType { get; set; }
        public string GroupID { get; set; }
        public string ClientType { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        [Required(ErrorMessage = "National ID is required")]
        public string NationalID { get; set; }
        public string Phone1 { get; set; }
        [Required(ErrorMessage = "Mobile is required")]
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string ApprovalStatus { get; set; }
        public string IsBlocked { get; set; }
        public string BlockedBy { get; set; }
        public string BlockedDate { get; set; }
        public string BlockedReason { get; set; }
        public List<IndividualContactsListModel> individualContactsList { get; set; }
        public string totalCount { get; set; }
    }
    public class IndividualContactsListModel
    {
        public string IsAdd { get; set; }
        public string Entry { get; set; }
        public int ContactID { get; set; }
        public string ClientID { get; set; }
        public string ServiceType { get; set; }
        public string GroupID { get; set; }
        public string ClientType { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }        
        public string NationalID { get; set; }
        public string Phone1 { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string ApprovalStatus { get; set; }
        public string IsBlocked { get; set; }
        public string BlockedBy { get; set; }
        public string BlockedDate { get; set; }
        public string BlockedReason { get; set; }
        public string ApprovedBy { get; set; }
        public string DateApproved { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string DateEdited { get; set; }
        public string EditedBy { get; set; }
    }
    public class GroupsModel
    {
        public List<GroupsListModel> groupsList { get; set; }
        public string totalCount { get; set; }
    }
    public class GroupsListModel
    {
        public string IsAdd { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
    }
    public class CustomerApprovalModel
    {        
        [Required]
        public string ContactID { get; set; }
        [Required]
        public string ServiceType { get; set; }
        [Required]
        public string Mobile { get; set; }
        public List<IndividualContactsListModel> Iclm { get; set; }
    }
}