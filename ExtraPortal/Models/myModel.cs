﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class sessionModel
    {
        public int level { get; set; }
        public string PhoneNumber { get; set; }

        public int getlevel
        {
            get
            {
                return level = false ? 0 :level;
            }
        }
    }
    public class UpdateSessionModel
    {
        public string session_id { get; set; }
        public string PhoneNumber { get; set; }
        public int level { get; set; }
    }
}