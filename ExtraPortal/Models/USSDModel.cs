﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class USSDModel
    {
    }
    public class DisableEnableUSSDSubscribersModel
    {
        [Required(ErrorMessage = "No Client was selected")]
        public string ContactID { get; set; }
        public string ClientID { get; set; }
        public string BlockedCount { get; set; }
        public string PendingApproval { get; set; }
        public List<IndividualContactsListModel> Iclm { get; set; }
        public List<IndividualContactsListModel> iclmlist { get; set; }
    }

    public class DisableUSSDSubscribersModel
    {
        [Required(ErrorMessage = "No Client was selected")]
        public string ContactID { get; set; }

        [Required(ErrorMessage = "Please enter Reason")]
        public string BlockedReason { get; set; }
    }

    public class EnableUSSDSubscribersModel
    {
        [Required(ErrorMessage = "No Client was selected")]
        public string ContactID { get; set; }
        [Required(ErrorMessage = "Mobile is required")]
        public string Mobile { get; set; }
    }

    public class PINManagementModel
    {
        [Required]
        public string ContactID { get; set; }
        [Required]
        [Display(Name = "Pin")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        public string Pin { get; set; }
        [Required]
        [Display(Name = "Confirm Pin")]
        [System.ComponentModel.DataAnnotations.Compare("Pin", ErrorMessage = "PIN confirmation must match New PIN.")]
        public string CPin { get; set; }
        public string Mobile { get; set; }
    }

    public class UserPINManagementModel
    {  
        public string Pin { get; set; }       
        public string CPin { get; set; }
        public string Mobile { get; set; }
    }
    public class ActivityLogModel
    {
        public List<ActivityLogListModel> activityLogList { get; set; }
        public string totalCount { get; set; }
    }
    public class ActivityLogListModel
    {
        public string SessionID { get; set; }
        public string IpAddress { get; set; }
        public string UserName { get; set; }
        public string TimeAccessed { get; set; }
    }
}