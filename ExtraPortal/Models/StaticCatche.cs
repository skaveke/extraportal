﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class StaticCatche
    {
        public void LoadStaticCache(accountModel info)
        {
            
            HttpContext.Current.Application["CustomerInfo"] = info;
            
        }


        public void LoadStaticTokenCache(string info)
        {

            HttpContext.Current.Application["TokenCode"] = info;

        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public accountModel GetCustomerDetails()
        {
            return HttpContext.Current.Application["CustomerInfo"] as accountModel;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public string GetTokenDetails()
        {
            return HttpContext.Current.Application["TokenCode"] as string;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public List<Clientaccounts> GetAccountDetails()
        {
            return HttpContext.Current.Application["ClientAccountLists"] as List<Clientaccounts>;
        }
    }
}