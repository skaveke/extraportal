﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraPortal.Models
{
    public class SystemModel
    {
    }
    public class BranchModel
    {
        public string OurBranchID { get; set; }
        public string BranchName { get; set; }
    }

    public class MainModuleModel
    {
        public int? MainModuleID { get; set; }
        public string MainModuleName { get; set; }
        public string MainModuleIcon { get; set; }
        public bool? isActive { get; set; }
        public int DisplayOrder { get; set; }

    }

    public class ModuleModel
    {
        public int? MainModuleID { get; set; }
        public int? ModuleID { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string ModuleName { get; set; }
        public string ModuleIcon { get; set; }
        public bool? isActive { get; set; }

    }

    public class UserAccessRightsModel
    {
        public string OurBranchID { get; set; }
        public string OperatorID { get; set; }
        public string RoleID { get; set; }
        public string greeting { get; set; }
        public string Year { get; set; }
        public IEnumerable<MainModuleModel> mainmodule { get; set; }
        public IEnumerable<ModuleModel> modules { get; set; }
    }
}