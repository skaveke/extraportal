﻿using System.Web.Mvc;

namespace ExtraPortal.Areas.SupremeMobile
{
    public class SupremeMobileAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SupremeMobile";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SupremeMobile_default",
                "SupremeMobile/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ExtraPortal.Areas.SupremeMobile.Controllers" }
            );
        }
    }
}