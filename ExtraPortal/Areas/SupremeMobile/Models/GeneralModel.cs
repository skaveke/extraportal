﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Areas.SupremeMobile.Models
{
    public class RegisterDevicesModel
    {
        public string UserName { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceID { get; set; }
        public string AndroidVersion { get; set; }
        public string DeviceNumber { get; set; }


    }
    public class RegisterDevicesModelResult
    {
        public string UserName { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceID { get; set; }
        public string AndroidVersion { get; set; }
        public string DeviceNumber { get; set; }

    }

    public class SubCodeModel
    {
        public string SubCodeID { get; set; }
        public string Description { get; set; }
    }

    public class SysCodeModel
    {
        [Required(ErrorMessage = "Token Code is required")]
        public string TokenCode { get; set; }
        public string SubCodeID { get; set; }
    }

    public class SysCodeDetailModel
    {
        public string CodeID { get; set; }
        public string Description { get; set; }
    }

    public class NewUsersModel
    {
        public Int32 UserID { get; set; }
        [Required(ErrorMessage = "User Name is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Our Branch ID is required")]
        public string OurBranchID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage = "CreatedBy is required")]
        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "Device ID is required")]
        public string DeviceID { get; set; }
        [Required(ErrorMessage = "Location is required")]
        public string Location { get; set; }
    }

    public class MyUsersModel
    {

        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string OurBranchID { get; set; }
        public string Password { get; set; }
        public string SecurityStamp { get; set; }
        public string IPAddress { get; set; }
        [Required(ErrorMessage = "Device ID is required")]
        public string DeviceID { get; set; }
        [Required(ErrorMessage = "Location is required")]
        public string Location { get; set; }
    }
    public class OnlineUsersModel
    {
        [Required(ErrorMessage = "Branch is required")]
        public string OurBranchID { get; set; }

    }
    public class AllocateDeviceIDModel
    {
        [Required(ErrorMessage = "UserName is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "DeviceID is required")]
        public string DeviceID { get; set; }
        [Required(ErrorMessage = "Remarks is required")]
        public string Remarks { get; set; }

        [Required(ErrorMessage = "CreatedBy is required")]
        public string CreatedBy { get; set; }



    }
    public class ChangePasswordModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string OurBranchID { get; set; }
        public string Password { get; set; }
        public string SecurityStamp { get; set; }
        public string IPAddress { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
    }

    public class ChangePasswordResultModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string OurBranchID { get; set; }
        public string Password { get; set; }

    }

    public class ShowOnlineUsersModel
    {
        public Int32 UserID { get; set; }
        public String UserName { get; set; }
        public string OurBranchID { get; set; }
        public string Password { get; set; }
        public string DeviceID { get; set; }
        public string SecurityStamp { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UserStatus { get; set; }
        public string TokenDate { get; set; }

    }

    public class CreatedUserResultModel
    {
        public Int32 ColumnID { get; set; }
        public Int32 UserID { get; set; }
        [Required(ErrorMessage = "User Name is required")]
        public string UserName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public string UserStatus { get; set; }
    }

    public class MyUserResultModel
    {
        public string Status { get; set; }
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
    }

    public class ValidTokenResultModel
    {
        public Int32 UserID { get; set; }
        public string UserName { get; set; }
        public string OurBranchID { get; set; }
        public string TokenDate { get; set; }
        public string SecurityStamp { get; set; }
    }

    public class MyTokenResultModel
    {
        public string tokenAuth { get; set; }
    }

    public class Audit
    {
        public Guid AuditID { get; set; }
        public string IPAddress { get; set; }
        public string UserName { get; set; }
        public string URLAccessed { get; set; }
        public DateTime TimeAccessed { get; set; }

        public Audit()
        {
        }
    }
}