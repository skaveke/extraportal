﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Reflection;

namespace ExtraPortal.Areas.SupremeMobile.Models
{
    
    public class User
    {
        public Int32 UserID { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }

    class CustomMembershipProvider : MembershipProvider
    {
        private int _minRequiredPasswordLength;

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);            
        }
        
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        //User authentication. returns true or false
        public override bool ValidateUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public MembershipUser CreateNewUser(string username, string password, string firstname, string lastname, string email, string mobile, int roleID, string createdby, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public MembershipUser UpdateUser(string username, string firstname, string lastname, string email, string mobile, int roleID, string createdby, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public MembershipUser UpdatePassword(int userID, string username, string password, string createdby, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string Loginname, bool userIsOnline)
        {
            object providerUserKey = Loginname;
            string username = Loginname;
            string email = "";

            string passwordQuestion = "";

            string comment = "";

            bool isApproved = false;

            bool isLockedOut = new Boolean();

            DateTime creationDate = new DateTime(); ;// DateTime.Parse(dv[0]["CreatedOn"].ToString());

            DateTime lastLoginDate = new DateTime();

            DateTime lastActivityDate = new DateTime();

            DateTime lastPasswordChangedDate = new DateTime();

            DateTime lastLockedOutDate = new DateTime();

            MembershipUser u = new MembershipUser("CustomMembershipProvider", username, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate,
            lastActivityDate, lastPasswordChangedDate, lastLockedOutDate);

            return u;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get {
                return 6; 
            }            
        }        

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        private MembershipUser GetUserFromDataTable(DataTable dtable)
        {
            DataView dv = new DataView(dtable);

            object providerUserKey = dv[0]["ID"];
            string username = dv[0]["UserName"].ToString();
            string email = dv[0]["AccountID"].ToString();

            string passwordQuestion = "";
            
            string comment = "";

            bool isApproved = false;

            bool isLockedOut = new Boolean();

            DateTime creationDate = new DateTime(); ;// DateTime.Parse(dv[0]["CreatedOn"].ToString());

            DateTime lastLoginDate = new DateTime();

            DateTime lastActivityDate = new DateTime();

            DateTime lastPasswordChangedDate = new DateTime();

            DateTime lastLockedOutDate = new DateTime();

            MembershipUser u = new MembershipUser("CustomMembershipProvider", username, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate,
            lastActivityDate, lastPasswordChangedDate, lastLockedOutDate);

            return u;
        }

        //public static DataTable ToDataTable<T>(List<T> items)
        //{
        //    DataTable dataTable = new DataTable(typeof(T).Name);

        //    //Get all the properties
        //    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        //    foreach (PropertyInfo prop in Props)
        //    {
        //        //Setting column names as Property names
        //        dataTable.Columns.Add(prop.Name);
        //    }
        //    foreach (T item in items)
        //    {
        //        var values = new object[Props.Length];
        //        for (int i = 0; i < Props.Length; i++)
        //        {
        //            //inserting property values to datatable rows
        //            values[i] = Props[i].GetValue(item, null);
        //        }
        //        dataTable.Rows.Add(values);
        //    }
        //    //put a breakpoint here and check datatable
        //    return dataTable;
        //}
    }
}
