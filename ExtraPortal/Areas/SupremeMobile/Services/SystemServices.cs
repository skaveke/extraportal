﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using ExtraPortal.Areas.SupremeMobile.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ExtraPortal.Areas.SupremeMobile.Services
{
    public class SystemServices
    {
        private const string _alg = "HmacSHA256";
        private const string _salt = "2eplSzM8CXnXyNAvRJ7j";
        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }

        //App DB connection      
        public static IDbConnection DbCon()
        {
            IDbConnection _db = new SqlConnection(DBConnection());

            return _db;
        }

        //Bantu DB connection
        public static IDbConnection B_DbCon()
        {
            IDbConnection _db = new SqlConnection(BantuDBConnection());

            return _db;
        }

        public static string BantuDBConnection()
        {
            try
            {
                return "Data Source=" + ConfigurationManager.AppSettings["B_DataSource"] +
                        ";Initial Catalog=" + ConfigurationManager.AppSettings["B_DBName"] +
                        ";User ID=" + ConfigurationManager.AppSettings["B_UName"] +
                        ";Password=" + ConfigurationManager.AppSettings["B_Pass"];
            }
            catch
            {
                return "";
            }

        }

        public static string DBConnection()
        {
            try
            {
                return "Data Source=" + ConfigurationManager.AppSettings["DataSource"] +
                        ";Initial Catalog=" + ConfigurationManager.AppSettings["DBName"] +
                        ";User ID=" + ConfigurationManager.AppSettings["UName"] +
                        ";Password=" + ConfigurationManager.AppSettings["Pass"];
            }
            catch
            {
                return "";
            }

        }

        public static string EncodePassword(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}