﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Areas.SupremeMobile.Models;
using ExtraPortal.Areas.SupremeMobile.Services;
using Newtonsoft.Json;
using Dapper;
using System.Data;
using ExtraPortal.Areas.SupremeMobile.Managers;

namespace ExtraPortal.Areas.SupremeMobile.Controllers
{
    public class SecurityController : Controller
    {
        // GET: SupremeMobile/Security
        private const string ResetPasswordTokenPurpose = "RP";
        private const string ConfirmLoginTokenPurpose = "LC";//change here change bit length for reason  section (2 per char)

        IDbConnection _db = SystemServices.DbCon();//App DB connection 

        static IDbConnection _bdb = SystemServices.B_DbCon();//Bantu DB connection

        SystemServices _SystemServices = new SystemServices();
        GeneralService logger = new GeneralService();
        [HttpPost]
        public JsonResult CreateAPIUser(NewUsersModel nUsermodel)
        {
            string errMessage = string.Empty;

            if (ModelState.IsValid)
            {

                try
                {
                    var newUserResult = _bdb.Query<CreatedUserResultModel>(";Exec Supreme_InsertUser @UserName,@OurBranchID,@Password,@CompanyName,@Address,@CreatedBy,@DeviceID,@Location",
                        new
                        {
                            UserName = nUsermodel.UserName,
                            OurBranchID = nUsermodel.OurBranchID,
                            Password = SystemServices.GetHashedPassword(nUsermodel.Password),
                            CompanyName = nUsermodel.CompanyName,
                            Address = nUsermodel.Address,
                            CreatedBy = nUsermodel.CreatedBy,
                            DeviceID = nUsermodel.DeviceID,
                            Location = nUsermodel.Location
                        }).SingleOrDefault();

                    nUsermodel.Password = string.Empty;
                    logger.LogWrite(JsonConvert.SerializeObject(nUsermodel).ToString());
                    return Json(newUserResult, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    GeneralService.WriteErrorLog(ref ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;
                logger.LogWrite(message);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChangePass(ChangePasswordModel model)
        {
            string errMessage = string.Empty;

            if (ModelState.IsValid)
            {

                try
                {
                    var newUserResult = _bdb.Query<CreatedUserResultModel>(";Exec EditSupremeUsers @UserName,@Password,@Remarks,@CreatedBy,@CreatedOn",
                        new
                        {
                            UserName = model.UserName,
                            Password = SystemServices.GetHashedPassword(model.Password),
                            Remarks = model.Remarks,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                        }).SingleOrDefault();

                    model.Password = string.Empty;
                    logger.LogWrite(JsonConvert.SerializeObject(model).ToString());
                    return Json(newUserResult, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    GeneralService.WriteErrorLog(ref ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;
                logger.LogWrite(message);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ShowActiveUser(OnlineUsersModel models)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var onlineUsersResult = _bdb.Query<ShowOnlineUsersModel>(";Exec Supreme_Online_Users @OurBranchID",
                        new
                        {
                            OurBranchID = models.OurBranchID
                        });


                    logger.LogWrite(JsonConvert.SerializeObject(models).ToString());
                    return Json(onlineUsersResult, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    GeneralService.WriteErrorLog(ref ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;
                logger.LogWrite(message);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AllocateDeviceID(AllocateDeviceIDModel models)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var onlineUsersResult = _bdb.Query<AllocateDeviceIDModel>(";Exec Supreme_AllocateDeviceID @UserName,@DeviceID,@Remarks,@CreatedBy",
                        new
                        {
                            UserName = models.UserName,
                            DeviceID = models.DeviceID,
                            Remarks = models.Remarks,
                            CreatedBy = models.CreatedBy,

                        });


                    logger.LogWrite(JsonConvert.SerializeObject(models).ToString());
                    return Json(onlineUsersResult, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    GeneralService.WriteErrorLog(ref ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;
                logger.LogWrite(message);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult APILogin(MyUsersModel Tokenmodel)
        {
            string EPassword = null;
            EPassword = SystemServices.EncodePassword(Tokenmodel.UserName + Tokenmodel.Password).ToString();//Encrypt Password
            try
            {
                var loginResult = _bdb.Query<MyUserResultModel>(";Exec Supreme_AuthenticateUser @UserName,@OurBranchID,@Password,@Location,@DeviceiD",
                new
                {
                    UserName = Tokenmodel.UserName,
                    OurBranchID = Tokenmodel.OurBranchID,
                    Password = EPassword,
                    Location = Tokenmodel.Location,
                    DeviceID = Tokenmodel.DeviceID
                }).SingleOrDefault();

                Tokenmodel.UserID = loginResult.UserID;
                Tokenmodel.SecurityStamp = Guid.NewGuid().ToString();

                var token = Managers.TokenManager.GenerateToken(ConfirmLoginTokenPurpose, Tokenmodel);

                _bdb.Query(";Exec Supreme_UpdateUserToken @UserID,@TokenID,@SecurityStamp",
                    new
                    {
                        UserID = Tokenmodel.UserID,
                        TokenID = token,
                        SecurityStamp = Tokenmodel.SecurityStamp
                    });

                MyTokenResultModel tokenResult = new MyTokenResultModel();
                tokenResult.tokenAuth = token;

                Tokenmodel.Password = string.Empty;
                logger.LogWrite(JsonConvert.SerializeObject(Tokenmodel).ToString());
                return Json(tokenResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = ee.Message;
                GeneralService.WriteErrorLog(ref ee);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }

        public static TokenValidation ValidateToken(string validToken)
        {
            try
            {
                var loginResult = _bdb.Query<ValidTokenResultModel>(";Exec Supreme_AuthenticateToken @TokenID",
                    new
                    {
                        TokenID = validToken
                    }).SingleOrDefault();

                MyUsersModel userModel = new MyUsersModel();
                userModel.UserID = loginResult.UserID;
                userModel.UserName = loginResult.UserName;
                userModel.OurBranchID = loginResult.OurBranchID;
                userModel.SecurityStamp = loginResult.SecurityStamp;

                GeneralService logger = new GeneralService();
                logger.LogWrite("Token: " + validToken + " - " + JsonConvert.SerializeObject(userModel).ToString());
                var validation = TokenManager.ValidateToken(ConfirmLoginTokenPurpose, userModel, validToken);

                return validation;
            }
            catch (Exception ee)
            {
                var result = new TokenValidation();
                result.Errors.Add(TokenValidationStatus.Expired);
                GeneralService.WriteErrorLog(ref ee);
                return result;
            }
        }

        public static string GetValidOperator(string validToken)
        {
            return TokenManager.GetValidOperatorID(ConfirmLoginTokenPurpose, validToken);

        }

        public JsonResult RegisterDevices(RegisterDevicesModel models)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var onlineUsersResult = _bdb.Query<RegisterDevicesModelResult>(";Exec Add_Device @DeviceNumber,@DeviceModel,@DeviceID,@AndroidVersion",
                        new
                        {
                            DeviceNumber = models.DeviceNumber,
                            DeviceModel = models.DeviceModel,
                            DeviceID = models.DeviceID,
                            AndroidVersion = models.AndroidVersion

                        });


                    logger.LogWrite(JsonConvert.SerializeObject(models).ToString());
                    return Json(onlineUsersResult, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    GeneralService.WriteErrorLog(ref ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;
                logger.LogWrite(message);
                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
        }
        
    }
}