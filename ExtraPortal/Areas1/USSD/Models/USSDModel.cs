﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExtraPortal.Areas.USSD.Models
{
    public class USSDModel
    {
    }
    public class PINManagementModel
    {
        [Required]
        public string ContactID { get; set; }
        [Required]
        [Display(Name = "Pin")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        public string Pin { get; set; }
        [Required]
        [Display(Name = "Confirm Pin")]
        [System.ComponentModel.DataAnnotations.Compare("Pin", ErrorMessage = "PIN confirmation must match New PIN.")]
        public string CPin { get; set; }
        public string Mobile { get; set; }
    }

    public class UserPINManagementModel
    {
        public string Pin { get; set; }
        public string CPin { get; set; }
        public string Mobile { get; set; }
    }

}