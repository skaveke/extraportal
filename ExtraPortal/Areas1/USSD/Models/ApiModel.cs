﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraPortal.Areas.USSD.Models
{
    public class ClientExist
    {
        public bool? IsActive { get; set; }
        public bool? IsBlocked { get; set; }
        public bool? ApprovalStatus { get; set; }
        public string Name { get; set; }
        public string ClientID { get; set; }
        public int? UpdateCount { get; set; }
    }

    public class pinModel
    {
        public string Pin { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }

    }

}