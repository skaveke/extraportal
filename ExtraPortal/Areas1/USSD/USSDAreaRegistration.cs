﻿using System.Web.Mvc;

namespace ExtraPortal.Areas.USSD
{
    public class USSDAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "USSD";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "USSD_default",
                "USSD/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ExtraPortal.Areas.USSD.Controllers" }
            );
        }
    }
}