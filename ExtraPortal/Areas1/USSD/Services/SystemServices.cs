﻿using Library.ErrorLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ExtraPortal.Areas.USSD.Services
{
    public class SystemServices
    {
        private const string _alg = "HmacSHA256";
        private const string _salt = "2eplSzM8CXnXyNAvRJ7j";
        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }

        //App DB connection      
        public static IDbConnection DbCon()
        {
            IDbConnection _db = new SqlConnection(DBConnection());

            return _db;
        }

        //Bantu DB connection
        public static IDbConnection B_DbCon()
        {
            IDbConnection _db = new SqlConnection(BantuDBConnection());

            return _db;
        }

        public static string BantuDBConnection()
        {
            try
            {
                return "Data Source=" + ConfigurationManager.AppSettings["B_DataSource"] +
                        ";Initial Catalog=" + ConfigurationManager.AppSettings["B_DBName"] +
                        ";User ID=" + ConfigurationManager.AppSettings["B_UName"] +
                        ";Password=" + ConfigurationManager.AppSettings["B_Pass"];
            }
            catch
            {
                return "";
            }

        }

        public static string DBConnection()
        {
            try
            {
                return "Data Source=" + ConfigurationManager.AppSettings["DataSource"] +
                        ";Initial Catalog=" + ConfigurationManager.AppSettings["DBName"] +
                        ";User ID=" + ConfigurationManager.AppSettings["UName"] +
                        ";Password=" + ConfigurationManager.AppSettings["Pass"];
            }
            catch
            {
                return "";
            }

        }

        //Bantu Encryption
        static public string EncryptText(string strInputText)
        {
            byte[] data = Array.ConvertAll<char, byte>(strInputText.ToCharArray(), delegate (char ch) { return (byte)ch; });
            SHA256 shaM = new SHA256Managed();
            byte[] result = shaM.ComputeHash(data);
            return Convert.ToBase64String(result);
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        //ErrorLog oErrorLog = new ErrorLog();
        private static string LogDirectory = String.Empty;

        static public bool WriteErrorLog(string logMessage)
        {
            bool successStatus = false;
            LogDirectory = ConfigurationManager.AppSettings["LogPath"].ToString();
            DateTime currentDateTime = DateTime.Now;
            string currentDateTimeString = currentDateTime.ToString();
            CheckCreateLogDirectory(LogDirectory);
            string logLine = BuildLogLine(currentDateTime, logMessage);
            LogDirectory = (LogDirectory + "Log_" + LogFileName(DateTime.Now) + ".txt");
            lock (typeof(ErrorLog))
            {
                StreamWriter m_logSWriter = null;
                try
                {
                    m_logSWriter = new StreamWriter(LogDirectory, true);
                    m_logSWriter.WriteLine(logLine);
                    successStatus = true;
                }
                catch
                {
                }
                finally
                {
                    if (m_logSWriter != null)
                    {
                        m_logSWriter.Close();
                    }
                }
            }
            return successStatus;
        }

        private static bool CheckCreateLogDirectory(string logPath)
        {
            bool loggingDirectoryExists = false;
            DirectoryInfo dirInfo = new DirectoryInfo(logPath);
            if (dirInfo.Exists)
            {
                loggingDirectoryExists = true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(logPath);
                    loggingDirectoryExists = true;
                }
                catch
                {
                    // Logging failure
                }
            }
            return loggingDirectoryExists;
        }

        private static string BuildLogLine(DateTime currentDateTime, string logMessage)
        {
            StringBuilder loglineStringBuilder = new StringBuilder();
            loglineStringBuilder.Append(LogFileEntryDateTime(currentDateTime));
            loglineStringBuilder.Append("\t");
            loglineStringBuilder.Append(logMessage);
            return loglineStringBuilder.ToString();
        }
        
        public static string LogFileEntryDateTime(DateTime currentDateTime)
        {
            return currentDateTime.ToString("dd-MM-yyyy HH:mm:ss");
        }

        private static string LogFileName(DateTime currentDateTime)
        {
            return currentDateTime.ToString("dd_MM_yyyy");
        }

        static public string GetInternalIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public string GetIpAddress()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            string ipaddress = Convert.ToString(ipEntry.AddressList[2]);

            return ipaddress.ToString();

        }
    }
}