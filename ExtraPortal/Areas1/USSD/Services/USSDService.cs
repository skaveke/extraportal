﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ExtraPortal.Areas.USSD.Models;
using Dapper;

namespace ExtraPortal.Areas.USSD.Services
{
    public class USSDService
    {
        IDbConnection _db = SystemServices.DbCon();

        static IDbConnection _bdb = SystemServices.B_DbCon();
        public string UserChangePIN(UserPINManagementModel model)
        {
            string EPin = null;
            EPin = Library.EncodePassword(DataTypeExtensions.Right(model.Mobile, 9) + model.Pin).ToString();//Encrypt PIN

            try
            {
                var message = _db.Query<int>(";Exec UserUSSDChangePIN @Mobile,@Pin,@EPin",
                    new
                    {
                        Mobile = model.Mobile,
                        Pin = model.Pin,
                        EPin = EPin

                    }).SingleOrDefault();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

    }
}