﻿using ExtraPortal.Managers;
using ExtraPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ExtraPortal.Services;

namespace ExtraPortal.Controllers
{
    public class AuthController : Controller
    {
        #region loading
        public const string ResetPasswordTokenPurpose = "RP";
        private const string ConfirmLoginTokenPurpose = "LC";
        SecurityService seService = new SecurityService();
        USSDService _USSDService = new USSDService();
        public ActionResult Index()
        {
            string mypass = Library.EncodePassword("1234");
            Console.WriteLine(mypass);
            return Content("Hi there dude!");
        }
        #endregion

        #region system Security
        [HttpPost]
        [Audit]
        public JsonResult ApiLogin(UserModel Tokenmodel)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    
                    var loginResult = seService.fetchClientPIN(Tokenmodel.UserName);

                    if (Library.EncodePassword(DataTypeExtensions.Right(Tokenmodel.UserName, 9)+Tokenmodel.Password) == loginResult.Pin)
                    { 
                        Tokenmodel.UserID = 1;

                    Tokenmodel.SecurityStamp = Guid.NewGuid().ToString();

                    var token = Managers.TokenManager.GenerateToken(ConfirmLoginTokenPurpose, Tokenmodel);

                    seService.TokenUpdate(Tokenmodel, token);

                    MyTokenResultModel tokenResult = new MyTokenResultModel();
                    tokenResult.tokenAuth = SecurityManager.ConvertStringToHex(token); 


                    Tokenmodel.Password = string.Empty;
                    FormsAuthentication.SetAuthCookie(token.ToString(), false);

                        return Json(tokenResult, JsonRequestBehavior.AllowGet);
                    }


                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = "Wrong Pin Entered";

                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [Audit]
        public JsonResult Login()
        {
            TokenResponse response = new TokenResponse
            {
                Status = "Failed",
                Message = "Wrong Token Entered"
            };
            return base.Json(response, 0);
        }
        
        #endregion

        #region  client operations
        [HttpPost]
        [Audit]
        public JsonResult ClientExist(string _PhoneNumber)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    var loginResult = seService.ClientCheck(_PhoneNumber);

                    return Json(loginResult, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        [Audit]
        [MyAuthorize]
        public JsonResult ClientInfo(string ClientID)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    var loginResult = seService.fetchClientDetail(ClientID);

                    return Json(loginResult, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        [Audit]
        [MyAuthorize]
        public JsonResult ClientAccounts(ClientRequest model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    var loginResult = seService.fetchAccounts(model);

                    return Json(loginResult, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region Token validation
        public static TokenValidation ValidateToken(string validToken)
        {
            try
            {
                SecurityService dsd = new SecurityService();

                var loginResult = dsd.TokenAuthenticate(validToken);

                UserModel userModel = new UserModel();
                userModel.UserID = 1;
                userModel.UserName = loginResult.UserName;
                userModel.SecurityStamp = loginResult.SecurityStamp;

                var validation = TokenManager.ValidateToken(ConfirmLoginTokenPurpose, userModel, validToken);

                return validation;
            }
            catch (Exception ee)
            {
                var result = new TokenValidation();
                result.Errors.Add(TokenValidationStatus.Expired);
                Library.WriteErrorLog(ee);
                return result;
            }
        }

        public static string GetValidOperator(string validToken)
        {
            return TokenManager.GetValidOperatorID(ConfirmLoginTokenPurpose, validToken);
        }
        #endregion

        #region Statement

        [HttpPost]
        [Audit]
        [MyAuthorize]
        public JsonResult AccountsStatement(StatementRequest model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    var loginResult = seService.fetchAccountsStatement(model.AccountID);

                    return Json(loginResult, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        [HttpPost]
        [Audit]
        public JsonResult ChangePIN(UserPINManagementModel model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                GenericResultModel AccListResult2 = new GenericResultModel();
                AccListResult2.Status = "Fail";
                AccListResult2.Remarks = message;

                return Json(AccListResult2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    var result = _USSDService.UserChangePIN(model);

                    if (result == "0")
                    {
                        var success = "Success";
                        return Json(success, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var fail = "";
                        return Json(fail, JsonRequestBehavior.AllowGet);
                    }

                    //return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ee)
                {
                    GenericResultModel AccListResult2 = new GenericResultModel();
                    AccListResult2.Status = "Fail";
                    AccListResult2.Remarks = ee.Message;
                    Library.WriteErrorLog(ee);
                    return Json(AccListResult2, JsonRequestBehavior.AllowGet);
                }
            }
        }
        
    }
}