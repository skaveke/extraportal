﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;
using System.Net;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class DeviceManagementController : Controller
    {
        // GET: DeviceManagement
        DeviceManagementServices _DeviceManagementServices = new DeviceManagementServices();

        [Audit]
        public ActionResult SupremeDevices()
        {
            SupremeDevicesModel _SupremeDevicesModel = new SupremeDevicesModel();
            _SupremeDevicesModel.supremeDevicesList = _DeviceManagementServices.GetSupremeDevices();
            TempData["MenuID"] = "Supreme Devices";
            ViewBag.PageTitle = "Supreme Devices";
            return View(_SupremeDevicesModel);
        }

        [HttpGet]
        [Audit]
        public ActionResult GetSupremeDeviceDetails(string DeviceNumber)
        {
            SupremeDevicesListModel _SupremeDevicesListModel = _DeviceManagementServices.GetSupremeDevices().Find(Rdr => Rdr.DeviceNumber == DeviceNumber);
            return Json(_SupremeDevicesListModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveEditSupremeDevice(SupremeDevicesModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _DeviceManagementServices.SaveEditSupremeDevice(model);

                if (message == "0")
                {
                    var success = "1";
                    return Json(success, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}