﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Models;
using ExtraPortal.Services;

namespace ExtraPortal.Controllers
{
    public class SystemController : Controller
    {
        SystemService _SystemService = new SystemService();
        // GET: System
          
        [ChildActionOnly]
        public ActionResult ModuleAccess()
        {

            UserAccessRightsModel _AccessModel = new UserAccessRightsModel();
            _AccessModel.mainmodule = _SystemService.MainModules();
            _AccessModel.modules = _SystemService.Modules();

            //AccessModel.OperatorID = Session["KE_UserName"].ToString();
            //AccessModel.RoleID = Session["KE_RoleID"].ToString();

            //if (DateTime.Now.Hour < 12)
            //{
            //    AccessModel.greeting = "Good Morning!";
            //}
            //else if (DateTime.Now.Hour < 17)
            //{
            //    AccessModel.greeting = "Good Afternoon!";
            //}
            //else
            //{
            //    AccessModel.greeting = "Good Evening!";
            //}
            //AccessModel.Year = DateTime.Now.Year.ToString();
            return PartialView("~/Views/Shared/_NavigationPartial.cshtml", _AccessModel);
        }
    }
}