﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;
using System.Linq.Dynamic;
using System.Net;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class AppUSSDController : Controller
    {
        // GET: AppUSSD
        USSDService _USSDService = new USSDService();        
        public ActionResult Subscribers()
        {
            TempData["MenuID"] = "Subscribers";
            ViewBag.PageTitle = "Subscribers";
            return View();
        }

        [HttpPost]
        [Audit]
        public ActionResult LoadData()
        {
            string search = Request.Form.GetValues("search[value]")[0];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            List<IndividualContactsListModel> _IndividualContactsListModel = _USSDService.GetUSSDSubscribers();

            if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
            {
                _IndividualContactsListModel = _IndividualContactsListModel.Where(
                    p => p.Name.ToLower().Contains(search.ToLower()) ||
                         p.ClientID.ToLower().Contains(search.ToLower())
                    ).ToList();
            }

            //SORT
            //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            //{
            //    _IndividualContactsListModel = _IndividualContactsListModel.OrderBy(sortColumn + " " + sortColumnDir);
            //}
            int recFilter = _IndividualContactsListModel.Count;
            recordsTotal = _IndividualContactsListModel.Count();
            var data = _IndividualContactsListModel.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recFilter, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);


        }

        [Audit]
        public ActionResult DisableEnable()
        {
            DisableEnableUSSDSubscribersModel _DisableEnableUSSDSubscribersModel = new DisableEnableUSSDSubscribersModel();
            _DisableEnableUSSDSubscribersModel.Iclm = _USSDService.GetDisabledUSSDSubscribersList();
            _DisableEnableUSSDSubscribersModel.BlockedCount = _USSDService.GetBlockedSubscribersCount();
            _DisableEnableUSSDSubscribersModel.iclmlist = _USSDService.GetPendingApproveEnabledUSSDSubscribersList();
            _DisableEnableUSSDSubscribersModel.PendingApproval = _USSDService.GetPendingApproveEnabledSubscribersCount();
            TempData["MenuID"] = "Disable/Enable";
            ViewBag.PageTitle = "Disable/Enable USSD Subscribers";

            return View(_DisableEnableUSSDSubscribersModel);
        }

        [HttpGet]
        public JsonResult GetUSSDSubscriberOption(string Prefix)
        {
            List<BantuContactsListModel> _BantuContactsListModel = _USSDService.GetUSSDSubscriberOption(Prefix);

            return Json(_BantuContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Audit]
        public ActionResult GetUSSDSubscriberDetails(string ClientID)
        {
            IndividualContactsListModel _IndividualContactsListModel = _USSDService.GetUSSDSubscriberDetails(ClientID);
            return Json(_IndividualContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult DisableUSSDSubscriber(DisableUSSDSubscribersModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to save
                try
                {
                    _USSDService.DisableUSSDSubscriber(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("DisableEnable");

                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Error"] = "Error";
                    return RedirectToAction("DisableEnable", model);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                TempData["ModelError"] = message;
                return RedirectToAction("DisableEnable");
            }

        }

        [HttpGet]
        [Audit]
        public JsonResult GetDisabledUSSDSubscriberOption(string Prefix)
        {
            List<BantuContactsListModel> _BantuContactsListModel = _USSDService.GetDisabledUSSDSubscriberOption(Prefix);

            return Json(_BantuContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [Audit]
        public ActionResult PINManagement()
        {
            TempData["MenuID"] = "PIN Management";
            ViewBag.PageTitle = "PIN Management";
            return View();
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePIN(PINManagementModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the Job Title
                try
                {
                    _USSDService.ChangePIN(model);

                    TempData["Success"] = "Success";
                    return RedirectToAction("PINManagement");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["ModelsError"] = e.Message;
                    return View("PINManagement", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("PINManagement");
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetDisabledUSSDSubscriberDetails(string ClientID)
        {
            IndividualContactsListModel _IndividualContactsListModel = _USSDService.GetDisabledUSSDSubscriberDetails(ClientID);
            return Json(_IndividualContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult EnableUSSDSubscriber(EnableUSSDSubscribersModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to save
                try
                {
                    _USSDService.EnableUSSDSubscriber(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("DisableEnable");

                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Error"] = "Error";
                    return RedirectToAction("DisableEnable", model);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                TempData["ModelError"] = message;
                return RedirectToAction("DisableEnable");
            }

        }
        
        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveUnblocking(CustomerApprovalModel model)
        {

            if (ModelState.IsValid)
            {
                var message = _USSDService.ApproveUnblocking(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("DisableEnable");
                }
                else
                {
                    TempData["ModelError"] = message;

                    return RedirectToAction("DisableEnable");
                }

            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("DisableEnable");
            }

        }

    }
}