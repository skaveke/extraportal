﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class HomeController : Controller
    {
        public ActionResult Dashboard()
        {
            TempData["MenuID"] = "Dashboard";
            ViewBag.PageTitle = "Dashboard";
            return View();
        }

        
    }
}