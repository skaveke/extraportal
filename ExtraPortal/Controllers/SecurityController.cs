﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Models;
using ExtraPortal.Services;
using System.Net;
using ExtraPortal.Managers;
using System.Data;
using System.Web.Security;
using Newtonsoft.Json;

namespace ExtraPortal.Controllers
{
    public class SecurityController : Controller
    {
        private string pass;
        private bool result = false;

        SystemTools _SystemTools = new SystemTools();
        SecurityService _SecurityService = new SecurityService();
        // GET: Security

        public const string ResetPasswordTokenPurpose = "RP";
        private const string ConfirmLoginTokenPurpose = "LC";
                
        public static string GetValidOperator(string validToken)
        {
            return TokenManager.GetValidOperatorID(ConfirmLoginTokenPurpose, validToken);

        }
        public ActionResult Index()
        {
            return View();
        }
        [Audit]
        public ActionResult Login()
        {
            LoginModel _LoginModel = new LoginModel();
            _LoginModel.BranchList = SystemService.SystemBranch("12");
            return View(_LoginModel);
        }

        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult WebLogin(LoginModel login)
        {
            login.BranchList = SystemService.SystemBranch("12");
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                ModelState.AddModelError("", message);
                return View("Login", login);
            }
            else
            {
                try
                {
                    var loginResult = SecurityService.UserAuthenticate(login);
                    pass = login.UserName.ToUpper() + login.Password;
                    result = (loginResult.Password.ToString() == SystemTools.EncryptText(pass));
                    if (result == true)
                    {

                        LoginUpdateModel tokenResult = new LoginUpdateModel();
                        tokenResult.OurBranchID = login.OurBranchID;
                        tokenResult.OperatorID = login.UserName.ToUpper();
                        tokenResult.Success = "S";
                        tokenResult.IPAddress = SystemTools.GetInternalIP();
                        tokenResult.MessageID = "";

                        SystemService.UpDateLogin(tokenResult);

                        login.Password = string.Empty;
                        SystemTools.WriteErrorLog(JsonConvert.SerializeObject(login).ToString());
                        FormsAuthentication.SetAuthCookie(loginResult.RoleID, false);
                        FormsAuthentication.SetAuthCookie(login.UserName.ToUpper(), false);
                        Session["KE_UserName"] = login.UserName.ToUpper();
                        Session["KE_RoleID"] = loginResult.RoleID;


                        return RedirectToAction("Dashboard", "Home");
                    }

                    return View("Login", login);
                }
                catch (Exception ee)
                {
                    SystemTools.WriteErrorLog(ee.ToString());
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", ee.Message);

                    return View("Login", login);

                }
            }
        }

        [Audit]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            Session["KE_UserName"] = null;
            Session["KE_RoleID"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon(); // it will clear the session at the end of request
            return RedirectToAction("Login");
        }
    }
}