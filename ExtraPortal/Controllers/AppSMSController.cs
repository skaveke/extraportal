﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;
using System.Net;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class AppSMSController : Controller
    {
        // GET: AppSMS
        SystemTools _SystemTools = new SystemTools();
        SystemService _SystemService = new SystemService();
        AppSMSService _AppSMSService = new AppSMSService();
        public ActionResult CustomSMS()
        {
            CustomSMSModel _CustomSMSModel = new CustomSMSModel();
            _CustomSMSModel.individualContact = _AppSMSService.GetIndividualContactOption();
            _CustomSMSModel.corporateContact = _AppSMSService.GetCorporateContactOption();
            _CustomSMSModel.groupContact = _AppSMSService.GetGroupContactOption();
            _CustomSMSModel.Bcsm = _AppSMSService.GetBantuContactOption();
            TempData["MenuID"] = "Custom SMS";
            return View(_CustomSMSModel);
        }

        [HttpPost]
        public ActionResult SendIndividualSMS(CustomSmsModel model)
        {
            if (ModelState.IsValid)
            {               
                // Attempt to register the Job Title
                try
                {
                    _AppSMSService.CreateCustomSMS(model);

                    TempData["Success"] = "Success";
                    return RedirectToAction("CustomSMS");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["ModelsError"] = e.Message;
                    return View("CustomSMS", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("CustomSMS");
            }
        }
        
        public ActionResult Category()
        {
            CategorySmsModel _CategorySmsModel = new CategorySmsModel();
            _CategorySmsModel.BranchList = SystemService.SystemBranch("12");
            _CategorySmsModel.productlist = _AppSMSService.GetSBProducts();
            _CategorySmsModel.chargeoption = _SystemService.GetSMSType();
            TempData["MenuID"] = "Category";
            return View(_CategorySmsModel);
        }
    }
}