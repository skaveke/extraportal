﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class AdministratorController : Controller
    {
        // GET: Administrator
        USSDService _USSDService = new USSDService();
        AdministratorService _AdministratorService = new AdministratorService();
        [Audit]
        public ActionResult ActivityLog()
        {
            TempData["MenuID"] = "Activity Log";
            ViewBag.PageTitle = "Activity Log";
            return View();
        }

        [HttpGet]
        [Audit]
        public ActionResult GetActivityLogList(PagingModel model)
        {
            ActivityLogModel _ActivityLogModel = new ActivityLogModel();
            _ActivityLogModel.activityLogList = _AdministratorService.GetActivityLogList(model);
            _ActivityLogModel.totalCount = _AdministratorService.GetActivityLogCount();
            return Json(_ActivityLogModel, JsonRequestBehavior.AllowGet);
        }
    }
}