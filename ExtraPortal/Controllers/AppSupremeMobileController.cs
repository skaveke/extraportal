﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;
using System.Net;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class AppSupremeMobileController : Controller
    {
        // GET: AppSupremeMobile
        SupremeMobileService _SupremeMobileService = new SupremeMobileService();

        [Audit]
        public ActionResult CreateSupremeUser()
        {
            SupremeUsersModel _SupremeUsersModel = new SupremeUsersModel();
            _SupremeUsersModel.BranchList = SystemService.SystemBranch("12");
            TempData["MenuID"] = "Create Supreme User";
            ViewBag.PageTitle = "Create Supreme User";
            return View(_SupremeUsersModel);
        }

        [Audit]
        public ActionResult SupremeUserApproval()
        {
            SupremeUsersModel _SupremeUsersModel = new SupremeUsersModel();
            _SupremeUsersModel.PendingSupremeUsersList = _SupremeMobileService.GetPendingSupremeUsers();
            TempData["MenuID"] = "Approve User";
            ViewBag.PageTitle = "Approve User";
            return View(_SupremeUsersModel);
        }

        [Audit]
        public ActionResult SupremeUsers()
        {
            SupremeUsersModel _SupremeUsersModel = new SupremeUsersModel();
            _SupremeUsersModel.supremeUsersList = _SupremeMobileService.GetSupremeUsers();
            TempData["MenuID"] = "Supreme Users";
            ViewBag.PageTitle = "Supreme Users";
            return View(_SupremeUsersModel);
        }

        [HttpGet]
        public JsonResult GetBantuUsersOption(string Prefix)
        {
            List<SupremeUsersListModel> _SupremeUsersListModel = _SupremeMobileService.GetBantuUsersOption(Prefix);

            return Json(_SupremeUsersListModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Audit]
        public ActionResult GetBantuUserDetails(string Operator)
        {
            SupremeUsersListModel _SupremeUsersListModel = _SupremeMobileService.GetBantuUserDetails(Operator);
            return Json(_SupremeUsersListModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Audit(AuditingLevel = 2)]
        public JsonResult IsUserNameExists(string UserName)
        {
            return Json(CheckUserName(UserName));
        }

        public bool CheckUserName(string UserName)
        {
            var result = _SupremeMobileService.SupremeUsersList().Find(Rdr => Rdr.UserName == UserName);

            bool status;

            if (result != null)
            {
                //Already registered  
                status = false;
            }
            else
            {
                //Available to use  
                status = true;
            }

            return status;
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult InsertSupremeUser(SupremeUsersModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to save
                try
                {
                    _SupremeMobileService.CreateSupremeUser(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("CreateSupremeUser");

                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Error"] = "Error";
                    return RedirectToAction("CreateSupremeUser", model);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                TempData["ModelError"] = message;
                return RedirectToAction("CreateSupremeUser");
            }

        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveSupremeUser(SupremeUsersListModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _SupremeMobileService.ApproveSupremeUser(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("SupremeUserApproval");
                }
                else
                {
                    TempData["ModelError"] = message;
                    return RedirectToAction("SupremeUserApproval");
                }

            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("SupremeUserApproval");
            }            

        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult BlockSupremeUser(SupremeUsersListModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _SupremeMobileService.BlockSupremeUser(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("SupremeUsers");
                }
                else
                {
                    TempData["ModelError"] = message;
                    return RedirectToAction("SupremeUsers");
                }

            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("SupremeUsers");
            }

        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult UnblockSupremeUser(SupremeUsersListModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _SupremeMobileService.UnblockSupremeUser(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("SupremeUsers");
                }
                else
                {
                    TempData["ModelError"] = message;
                    return RedirectToAction("SupremeUsers");
                }

            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("SupremeUsers");
            }

        }

        [Audit]
        public ActionResult DeviceAssignment()
        {
            DeviceAssignmentModel _DeviceAssignmentModel = new DeviceAssignmentModel();
            _DeviceAssignmentModel.deviceAssignmentList = _SupremeMobileService.GetDevicesAssignedList();
            _DeviceAssignmentModel.supremeUsers = _SupremeMobileService.GetSupremeUserOption();
            _DeviceAssignmentModel.supremeDevices = _SupremeMobileService.GetSupremeDevicesOption();
            TempData["MenuID"] = "Device Assignment";
            ViewBag.PageTitle = "Device Assignment";
            return View(_DeviceAssignmentModel);
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult AssignUserDevice(DeviceAssignmentModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _SupremeMobileService.AssignUserDevice(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("DeviceAssignment");
                }
                else
                {
                    TempData["ModelError"] = message;
                    return RedirectToAction("DeviceAssignment");
                }

            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("DeviceAssignment");
            }

        }

    }
}