﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtraPortal.Services;
using ExtraPortal.Models;
using System.Net;

namespace ExtraPortal.Controllers
{
    [SecurityService.SessionTimeout]
    public class CRMController : Controller
    {
        // GET: CRM        
        CRMService _CRMService = new CRMService();

        [Audit]
        public ActionResult IndividualContacts()
        {
            TempData["MenuID"] = "Customer";
            ViewBag.PageTitle = "Customer Information";
            return View();
        }

        [HttpGet]
        public JsonResult BantuClientsOption(string Prefix)
        {
            List<BantuContactsListModel> _BantuContactsListModel = _CRMService.GetBantuContactsOption(Prefix);
           
            return Json(_BantuContactsListModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CorporateContacts()
        {
            TempData["MenuID"] = "Contacts";
            return View();
        }

        [HttpGet]
        [Audit]
        public ActionResult GetBantuContactsList(PagingModel model)
        {
            BantuContactsModel _BantuContactsModel = new BantuContactsModel();
            _BantuContactsModel.bantuContactsList = _CRMService.GetBantuContactsList(model);
            _BantuContactsModel.totalCount = _CRMService.GetBantuContactsCount();
            return Json(_BantuContactsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Audit]
        public ActionResult SaveEditIndividualContact(IndividualContactsModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _CRMService.SaveEditIndividualContact(model);

                if (message == "0")
                {
                    var success = "1";
                    return Json(success, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetIndividualContactsList(PagingModel model)
        {
            IndividualContactsModel _IndividualContactsModel = new IndividualContactsModel();
            _IndividualContactsModel.individualContactsList = _CRMService.GetIndividualContactsList(model);
            _IndividualContactsModel.totalCount = _CRMService.GetIndividualContactsCount();
            return Json(_IndividualContactsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Audit]
        public ActionResult GetIndividualContactDetails(int ContactID)
        {
            IndividualContactsListModel _IndividualContactsListModel = _CRMService.IndividualContactsList().Find(Rdr => Rdr.ContactID == ContactID);
            return Json(_IndividualContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [Audit]
        public ActionResult RemoveIndividualContactDetails(IndividualContactsListModel model)
        {
            var message = _CRMService.RemoveIndividualContactDetails(model);

            if (message == "0")
            {
                var success = "1";
                return Json(success, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetCorporateContactsList(PagingModel model)
        {
            IndividualContactsModel _IndividualContactsModel = new IndividualContactsModel();
            _IndividualContactsModel.individualContactsList = _CRMService.GetCorporateContactsList(model);
            _IndividualContactsModel.totalCount = _CRMService.GetCorporateContactsCount();
            return Json(_IndividualContactsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Audit]
        public ActionResult GetGroupsList(PagingModel model)
        {
            GroupsModel _GroupsModel = new GroupsModel();
            _GroupsModel.groupsList = _CRMService.GetGroupsList(model);
            _GroupsModel.totalCount = _CRMService.GetGroupsCount();
            return Json(_GroupsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Audit]
        public ActionResult SaveEditGroup(GroupsListModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _CRMService.SaveEditGroup(model);

                if (message == "0")
                {
                    var success = "1";
                    return Json(success, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetGroupDetails(int GroupID)
        {
            GroupsListModel _GroupsListModel = _CRMService.GetGroupDetails().Find(Rdr => Rdr.GroupID == GroupID);
            return Json(_GroupsListModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveGroup(GroupsListModel model)
        {
            var message = _CRMService.RemoveGroup(model);

            if (message == "0")
            {
                var success = "1";
                return Json(success, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetGroupContactsList(PagingModel model)
        {
            IndividualContactsModel _IndividualContactsModel = new IndividualContactsModel();
            _IndividualContactsModel.individualContactsList = _CRMService.GetGroupContactsList(model);
            _IndividualContactsModel.totalCount = _CRMService.GetGroupContactsCount(model.id);
            return Json(_IndividualContactsModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEditGroupContact(IndividualContactsListModel model)
        {
            if (ModelState.IsValid)
            {
                var message = _CRMService.SaveEditGroupContact(model);

                if (message == "0")
                {
                    var success = "1";
                    return Json(success, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetGroupContactDetails(int ContactID)
        {
            IndividualContactsListModel _IndividualContactsListModel = _CRMService.IndividualContactsList().Find(Rdr => Rdr.ContactID == ContactID);
            return Json(_IndividualContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [Audit]
        public ActionResult RemoveGroupContact(IndividualContactsListModel model)
        {
            var message = _CRMService.RemoveIndividualContactDetails(model);

            if (message == "0")
            {
                var success = "1";
                return Json(success, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Audit]
        public ActionResult GetClientDetails(string ClientID)
        {
            IndividualContactsListModel _IndividualContactsListModel = _CRMService.GetClientDetails(ClientID);
            return Json(_IndividualContactsListModel, JsonRequestBehavior.AllowGet);
        }

        [Audit]
        public ActionResult CustomerApproval()
        {
            CustomerApprovalModel _CustomerApprovalModel = new CustomerApprovalModel();
            _CustomerApprovalModel.Iclm = _CRMService.CustomerListPendingApproval();

            TempData["MenuID"] = "Customer Approval";
            ViewBag.PageTitle = "Customer Approval";
            return View(_CustomerApprovalModel);
        }

        [HttpPost]
        [Audit]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveCustomer(CustomerApprovalModel model)
        {

            if (ModelState.IsValid)
            {
                var message = _CRMService.ApproveCustomer(model);

                if (message == "0")
                {
                    TempData["Success"] = "Success";
                    return RedirectToAction("CustomerApproval");
                }
                else
                {
                    TempData["ModelError"] = message;

                    return RedirectToAction("CustomerApproval");
                }
                
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                TempData["ModelError"] = message;

                return RedirectToAction("CustomerApproval");
            }

        }

    }
}