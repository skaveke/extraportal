--Run this script on CBS
/****** Object:  Table [dbo].[Supreme_Users]    Script Date: 20-Jun-2018 11:08:11 ******/
DROP TABLE [dbo].[Supreme_Users]
GO

/****** Object:  Table [dbo].[Supreme_Users]    Script Date: 20-Jun-2018 11:08:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Supreme_Users](
	[UserID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClientID] [varchar](50) NULL,
	[EmployeeID] [varchar](50) NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](150) NULL,
	[CompanyName] [varchar](100) NULL,
	[Address] [varchar](100) NULL,
	[CreatedON] [datetime] NOT NULL CONSTRAINT [DF__Supreme_U__Creat__26E3CD7E]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[TokenID] [varchar](200) NULL,
	[TokenDate] [datetime] NULL,
	[SecurityStamp] [varchar](200) NULL,
	[UserStatus] [bit] NULL CONSTRAINT [DF_Supreme_Users_UserStatus]  DEFAULT ((1)),
	[OurBranchID] [varchar](3) NULL,
	[UserLocation] [varchar](100) NULL,
	[DeviceID] [varchar](100) NULL,
	[ApprovalStatus] [bit] NULL CONSTRAINT [DF_Supreme_Users_ApprovalStatus]  DEFAULT ((0)),
	[ApprovedBy] [nvarchar](100) NULL,
	[ApprovedDate] [datetime] NULL,
	[Blocked] [bit] NULL CONSTRAINT [DF_Supreme_Users_Blocked]  DEFAULT ((0)),
	[BlockedBy] [nvarchar](100) NULL,
	[BlockedDate] [datetime] NULL,
	[TerminusCreated] [nvarchar](200) NULL,
	[IpAddressCreated] [nvarchar](200) NULL,
 CONSTRAINT [PK_Supreme_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



--BEGIN: autosearch individual clients--
CREATE PROCEDURE [dbo].[GetBantuContactsOption]
	(
	@Prefix nvarchar(200)
	)	
AS
BEGIN
    --Autosearch Individual clients Only--	
	SELECT DISTINCT(t_Client.ClientID),t_Client.Name FROM t_Client 
	INNER JOIN t_AccountCustomer ON ((t_Client.ClientTypeID='I') AND (t_Client.ClientID = t_AccountCustomer.ClientID)) AND
	((t_Client.Name LIKE @Prefix + '%') OR (t_Client.ClientID LIKE @Prefix + '%') )
	ORDER BY t_Client.ClientID DESC;
END
--END: autosearch individual clients--


--//SUPREME MOBILE SP`s//---
--BEGIN: Get Users--
CREATE PROC GetSupremeUsers
AS
BEGIN
	select * from Supreme_Users Where UserStatus=1 AND ApprovalStatus=1 
END
--END: Get Users--

--BEGIN: Get Active Bantu Users--
CREATE PROCEDURE [dbo].[GetBantuUsersOption]
	(
	@Prefix nvarchar(200)
	)
	
AS
BEGIN
	select OperatorID,ClientID from t_User where  OperatorID LIKE @Prefix + '%' AND OperatorStatusID='A'
	
END
--END: Get Active Bantu Users--

--BEGIN: Get Bantu User Details--
CREATE PROCEDURE [dbo].[GetBantuUserDetails]
(
	@OperatorID nvarchar(100)
)
AS
BEGIN
	select ClientID,OperatorID,EmployeeID from t_User WHERE OperatorID=@OperatorID AND OperatorStatusID='A'
	
END

--END: Get Bantu User Details--

--BEGIN: Get Supreme Users List--
CREATE PROCEDURE [dbo].[GetSupremeUsersList]

AS
BEGIN
	select * from Supreme_Users WHERE UserStatus=1
	
END

--END: Get Supreme Users List--

--BEGIN: Create Supreme Users List--
ALTER PROCEDURE [dbo].[CreateSupremeUser]
	(
	@UserName nvarchar(100),
	@ClientID nvarchar(50),
	@EmployeeID nvarchar(50),
	@Password nvarchar(200),
	@OurBranchID nvarchar(5),
	@Author nvarchar(200),
	@Terminus nvarchar(200),
	@IpAddress nvarchar(200)
	)

AS
BEGIN
	INSERT INTO [dbo].[Supreme_Users]
           ([ClientID]
           ,[EmployeeID]
           ,[UserName]
           ,[Password]
		   ,[OurBranchID]
           ,[CreatedON]
           ,[CreatedBy] 
		   ,[TerminusCreated]
		   ,[IpAddressCreated] 
           )
     VALUES
           (@ClientID
           ,@EmployeeID
           ,@UserName
           ,@Password
		   ,@OurBranchID
           ,GETDATE()
           ,@Author
		   ,@Terminus
		   ,@IpAddress
           )
	
END


--END: Create Supreme Users List--

--BEGIN: Get Pending Supreme Users List--
CREATE PROC GetPendingSupremeUsers
AS
BEGIN
	select * from Supreme_Users Where UserStatus=1 AND ApprovalStatus=0 AND Blocked=0 
END
--END: Get Pending Supreme Users List--

--BEGIN: Approve Pending Supreme Users List--
CREATE PROC ApproveSupremeUser
	(
	@UserName nvarchar(100),
	@Author nvarchar(200),
	@Terminus nvarchar(200),
	@IpAddress nvarchar(200)
	)

AS
BEGIN
	
	IF EXISTS(SELECT CreatedBy from Supreme_Users where CreatedBy=@Author AND UserName=@UserName)
		BEGIN
			RAISERROR(N' You cant approve',16,1)
			RETURN
		END

	update Supreme_Users set 
	ApprovalStatus=1,
	ApprovedBy=@Author,
	ApprovedDate=GETDATE() 
	where UserName=@UserName
END

--END: Approve Pending Supreme Users List--

--BEGIN: Block Supreme User--
CREATE PROC BlockSupremeUser
	(
	@UserName nvarchar(100),
	@Author nvarchar(200),
	@Terminus nvarchar(200),
	@IpAddress nvarchar(200)
	)

AS
BEGIN
	update Supreme_Users set 
	Blocked=1,
	BlockedBy=@Author,
	BlockedDate=GETDATE()
	where UserName=@UserName
END
--END: Block Supreme User--

--BEGIN: Unblock Supreme User--
CREATE PROC UnblockSupremeUser
	(
	@UserName nvarchar(100),
	@Author nvarchar(200),
	@Terminus nvarchar(200),
	@IpAddress nvarchar(200)
	)

AS
BEGIN
	update Supreme_Users set 
	Blocked=0
	where UserName=@UserName
END
--END: Unblock Supreme User--

--BEGIN: Get Supreme Devices--
CREATE PROC GetSupremeDevices
AS
BEGIN
	select * from supreme_devices Where DeviceStatus=1
END
--END: Get Supreme Devices--

--BEGIN: Save/Edit Supreme Devices--
CREATE PROCEDURE [dbo].[SaveEditSupremeDevice]
	 @DeviceNumber bigint = NULL
	,@DeviceID nvarchar(100) = NULL
	,@DeviceModel nvarchar(100) = NULL
	,@ProductName nvarchar(100) = NULL
	,@AndroidVersion nvarchar(10)
	,@ProductType nvarchar(100)
	,@Memory nvarchar(50)
	,@Storage nvarchar(100)
	,@Speed nvarchar(50)
	,@Size nvarchar(50)
	,@Weight nvarchar(100)	
	,@Color nvarchar(100)
	,@Author nvarchar(100)
    ,@Terminus nvarchar(100)
    ,@IpAddress nvarchar(100)
	,@IsAdd int
AS
BEGIN
	if @IsAdd = 1 
		begin 
			INSERT INTO [dbo].[supreme_devices]
           ([DeviceNumber]
           ,[DeviceModel]
           ,[DeviceID]
           ,[AndroidVersion]
           ,[ProductName]
           ,[ProductType]
           ,[Memory]
           ,[Storage]
           ,[Speed]
           ,[Size]
           ,[Weight]
           ,[Color]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[TerminusCreated]
           ,[IpAddressCreated])
     VALUES
           (@DeviceNumber
           ,@DeviceModel
           ,@DeviceID
           ,@AndroidVersion
           ,@ProductName
           ,@ProductType
           ,@Memory
           ,@Storage
           ,@Speed
           ,@Size
           ,@Weight
           ,@Color
           ,@AndroidVersion
           ,GETDATE()
           ,@Terminus
		   ,@IpAddress)
		end 
	else
		begin
			BEGIN TRANSACTION UPDATETRECORD
				UPDATE [dbo].[supreme_devices]
				   SET [DeviceModel] = @DeviceModel
					  ,[AndroidVersion] = @AndroidVersion
					  ,[ProductName] = @ProductName
					  ,[ProductType] = @ProductType
					  ,[Memory] = @Memory
					  ,[Storage] = @Storage
					  ,[Speed] = @Speed
					  ,[Size] = @Size
					  ,[Weight] = @Weight
					  ,[Color] = @Color
					  ,[EditedBy] = @Author
					  ,[EditedDate] = GETDATE()
					  ,[TerminusEdited] = @Terminus
					  ,[IpAddressEdited] = @IpAddress
				 WHERE [DeviceNumber] = @DeviceNumber
 
			COMMIT TRANSACTION UPDATETRECORD
		end
END
--END: Save/Edit Supreme Devices--

--BEGIN: Get Supreme Users Option--
CREATE PROC GetSupremeUserOption
AS
BEGIN
	select UserName from Supreme_Users Where UserStatus=1 AND ApprovalStatus=1 
END
--END: Get Supreme Users Option--

--BEGIN: Get Supreme Devices Option--
CREATE PROC GetSupremeDevicesOption
AS
BEGIN
	select DeviceID, DeviceNumber + ' - ' + ProductName AS DeviceDetails from supreme_devices Where DeviceStatus=1
END
--END: Get Supreme Devices Option--

--BEGIN: Assign User Device--
CREATE PROC AssignUserDevice
	(
	@UserName nvarchar(100),
	@DeviceID nvarchar(100),
	@Author nvarchar(200),
	@Terminus nvarchar(200),
	@IpAddress nvarchar(200)
	)

AS
BEGIN
	--update Users table
	update Supreme_Users set 
	DeviceID = @DeviceID
	where UserName=@UserName

	--update Devices table
	update Supreme_Devices set 
	UserName = @UserName
	where DeviceID=@DeviceID

END
--END: Assign User Device--

--BEGIN: Get Devices Assigned Option--
CREATE PROC GetDevicesAssignedList
AS
BEGIN
	select Supreme_Users.UserName,Supreme_Users.DeviceID + ' ' + supreme_devices.ProductName As DeviceDetails 
	from Supreme_Users 
	INNER JOIN supreme_devices ON Supreme_Users.DeviceID = supreme_devices.DeviceID
	AND Supreme_Users.UserStatus=1 AND Supreme_Users.ApprovalStatus=1 AND Supreme_Users.DeviceID IS NOT NULL
	
END
--END: Get Devices Assigned Option--
-----
TABLE
User
Device
AssignedBy
DateAssigned
CreatedBy
DateCreated
TerminusCreated
IpAddressCreated
Status
IsBlocked
BlockedBy
ReasonBlocked

